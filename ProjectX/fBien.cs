﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectX
{
    class fBien
    {
        public string a
        {
            set;
            get;
        }
        public string b
        {
            set;
            get;
        }
        /// <summary>
        /// Nhiệt độ nước gia nhiệt vào	tH1	oC
        /// </summary>
        public string tH1
        {
            set;
            get;
        }
        /// <summary>
        /// Nhiệt độ nước ra nhiệt ra	tH2
        /// </summary>
        public string tH2
        {
            set;
            get;
        }
        /// <summary>
        /// Nhiệt độ nguồn gia nhiệt	tH	0C
        /// </summary>
        public string tH
        {
            set;
            get;
        }
        /// <summary>
        /// Nhiệt độ ngưng tụ tại dàn ngưng và bình hấp thụ	tK	0C
        /// </summary>
        public string tK
        {
            set;
            get;
        }
        /// <summary>
        /// Nhiệt độ bay hơi tại dàn bay hơi    t0	0C
        /// </summary>
        public string t0
        {
            set;
            get;
        }
        /// <summary>
        ///Nồng độ dung dịch đậm đặc   xd kg/kg
        /// </summary>
        public string Zd
        {
            set;
            get;
        }
        /// <summary>
        ///Áp suất ngưng tụ	pk	Bar
        /// </summary>
        public string Pk
        {
            set;
            get;
        }
        /// <summary>
        ///Áp suất bay hơi	p0	Bar
        /// </summary>
        public string P0
        {
            set;
            get;
        }
        /// <summary>
        /// ĐIỂM 10:
        ///Áp suất	p10	Bar
        /// </summary>
        public string P10
        {
            set;
            get;
        }
        /// <summary>
        /// ĐIỂM 10:
        /// Nhiệt độ	t10	0C
        /// </summary>
        public string t10
        {
            set;
            get;
        }
        /// <summary>
        /// ĐIỂM 10:
        ///Nồng độ dung dịch	x10	kg/kg
        /// </summary>
        public string Z10
        {
            set;
            get;
        }
        /// <summary>
        /// ĐIỂM 10:
        ///Entanpi dung dịch	i10	"kJ/kg (chia cho 1000 để ra kJ/kg)"
        /// </summary>
        public string i10
        {
            set;
            get;
        }
        /// <summary>
        /// ĐIỂM 7:
        ///Áp suất	p7	Bar
        /// </summary>
        public string P7
        {
            set;
            get;
        }
        /// <summary>
        /// ĐIỂM 7:
        ///Nhiệt độ	t7	oC
        /// </summary>
        public string t7
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 7:
        /// Nồng độ dung dịch	x7	kg/kg
        /// </summary>
        public string Z7
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 7:
        /// Entanpi dung dịch loãng	i7	kJ/kg
        /// </summary>
        public string i7
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 7:
        /// Vùng khử khí	Dx	kg/kg
        /// </summary>
        public string DZ
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 2:
        /// Áp suất	p2	Bar
        /// </summary>
        public string P2
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 2:
        /// Nhiệt độ	t2	oC
        /// </summary>
        public string t2
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 2:
        /// Nồng độ dung dịch	x2	kg/kg
        /// </summary>
        public string Z2
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 2:
        /// Entanpi	i2	KJ/kg
        /// </summary>
        public string i2
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 5:
        /// Áp suất	p5	Bar
        /// </summary>
        public string P5
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 5:
        /// Nhiệt độ	t5	oC
        /// </summary>
        public string t5
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 5:
        ///Nồng độ dung dịch	x5	kg/kg
        /// </summary>
        public string Z5
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 5:
        ///Entanpi	i5	kJ/kg
        /// </summary>
        public string i5
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 6:
        ///Áp suất	p6	Bar
        /// </summary>
        public string P6
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 6:
        ///Nhiệt độ	t6	oC
        /// </summary>
        public string t6
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 6:
        ///Nồng độ dung dịch	x6	kg/kg
        /// </summary>
        public string Z6
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 6:
        /// Entanpi	i6	KJ/kg
        /// </summary>
        public string i6
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 3:
        /// Áp suất	p3	Bar
        /// </summary>
        public string P3
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 3:
        ///Nhiệt độ	t3	oC
        /// </summary>
        public string t3
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 3:
        ///Nồng độ dung dịch	x3	kg/kg
        /// </summary>
        public string Z3
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 3:
        ///Entanpi	i3	J/kg
        /// </summary>
        public string i3
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 4:
        ///Áp suất	p4	Bar
        /// </summary>
        public string P4
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 4:
        ///Nhiệt độ	t4	oC
        /// </summary>
        public string t4
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 4:
        ///Nồng độ dung dịch	x4	kg/kg
        /// </summary>
        public string Z4
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 4:
        ///Entanpi	i4	J/kg
        /// </summary>
        public string i4
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 11:
        ///Áp suất	p11	Bar
        /// </summary>
        public string P11
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 11:
        ///Nhiệt độ	t11	oC
        /// </summary>
        public string t11
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 11:
        ///Nồng độ dung dịch	x11	kg/kg
        /// </summary>
        public string Z11
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 11:
        ///Entanpi	i11	J/kg
        /// </summary>
        public string i11
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 11:
        ///Zh
        /// </summary>
        public string Zh
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 14:
        ///Áp suất	p14	Bar
        /// </summary>
        public string P14
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 14:
        ///Nhiệt độ	t14	oC
        /// </summary>
        public string t14
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 14:
        ///Nồng độ dung dịch	x14	kg/kg
        /// </summary>
        public string Z14
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 14:
        ///Entanpi	i14	KJ/kg
        /// </summary>
        public string i14
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 15:
        ///Áp suất	p15	Bar
        /// </summary>
        public string P15
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 15:
        ///Nhiệt độ	t15	oC
        /// </summary>
        public string t15
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 15:
        ///Nồng độ dung dịch	x15	kg/kg
        /// </summary>
        public string Z15
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 15:
        ///Entanpi	i15	KJ/kg
        /// </summary>
        public string i15
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 15:
        ///Áp suất riêng phần của hơi nước	pH2O	Bar
        /// </summary>
        public string PH2O
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 15:
        ///Thành phần thể tích của hơi	rH20
        /// </summary>
        public string rH2O
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 1:
        ///Áp suất	p1	Bar
        /// </summary>
        public string P1
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 1:
        ///Nhiệt độ	t1	oC
        /// </summary>
        public string t1
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 1:
        ///Nồng độ dung dịch	x1	kg/kg
        /// </summary>
        public string Z1
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 1:
        ///Entanpi	i1	KJ/kg
        /// </summary>
        public string i1
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 12:
        ///Áp suất	p12	Bar
        /// </summary>
        public string P12
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 12:
        ///Nhiệt độ	t12	oC
        /// </summary>
        public string t12
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 12:
        ///Nồng độ dung dịch	x12	kg/kg
        /// </summary>
        public string Z12
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 12:
        ///Entanpi	i12	KJ/kg
        /// </summary>
        public string i12
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 8:
        ///Áp suất	p8	Bar
        /// </summary>
        public string P8
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 8:
        ///Nhiệt độ	t8	oC
        /// </summary>
        public string t8
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 8:
        ///Nồng độ dung dịch	x8	kg/kg
        /// </summary>
        public string Z8
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 8:
        ///Entanpi	i8	KJ/kg
        /// </summary>
        public string i8
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 13:
        ///Áp suất	p13	Bar
        /// </summary>
        public string P13
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 13:
        ///Nhiệt độ	t13	oC
        /// </summary>
        public string t13
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 13:
        ///Nồng độ dung dịch	x13	kg/kg
        /// </summary>
        public string Z13
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 13:
        ///Entanpi	i13	KJ/kg
        /// </summary>
        public string i13
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 9:
        ///Áp suất	p9	Bar
        /// </summary>
        public string P9
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 9:
        ///Nhiệt độ	t9	oC
        /// </summary>
        public string t9
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 9:
        ///Nồng độ dung dịch	x9	kg/kg
        /// </summary>
        public string Z9
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 9:
        ///Entanpi	i9	KJ/kg
        /// </summary>
        public string i9
        {
            set;
            get;
        }
        //------------------------------------ Điểm-----------------------------------------
        /// <summary>
        ///ĐIỂM 1
        /// </summary>
        public string diem1
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 2
        /// </summary>
        public string diem2
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 3
        /// </summary>
        public string diem3
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 4
        /// </summary>
        public string diem4
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 5
        /// </summary>
        public string diem5
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 6
        /// </summary>
        public string diem6
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 7
        /// </summary>
        public string diem7
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 8
        /// </summary>
        public string diem8
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 9
        /// </summary>
        public string diem9
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 10
        /// </summary>
        public string diem10
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 11
        /// </summary>
        public string diem11
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 12
        /// </summary>
        public string diem12
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 13
        /// </summary>
        public string diem13
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 14
        /// </summary>
        public string diem14
        {
            set;
            get;
        }
        /// <summary>
        ///ĐIỂM 15
        /// </summary>
        public string diem15
        {
            set;
            get;
        }
        
    }
}
