﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.SQLite;

namespace ProjectX
{
    class ham
    {
        public static double x;
        public static double fb;
        public static double fc;
        public static double fx;
        public static string value = "";
        SQLiteConnection conn = new SQLiteConnection();
        SQLiteDataAdapter adapter = new SQLiteDataAdapter();
        public string hammusau(double a6, double a5, double a4, double a3, double a2, double a1, double a, double b, double c, double s)
        {
            //Phuong trinh: a6 * x^6 + a5 * x^5 + a4 * x^4 + a3 * x^3 + a2 * x^2 + a1 * x + a = 0
            //(b-c) khoang cach ly nghiem;
            //s: khoang sai so

            fb = a6 * b * b * b * b * b * b + a5 * b * b * b * b * b + a4 * b * b * b * b + a3 * b * b * b + a2 * b * b + a1 * b + a;
            fc = a6 * c * c * c * c * c * c + a5 * c * c * c * c * c + a4 * c * c * c * c + a3 * c * c * c + a2 * c * c + a1 * c + a;

            if ((fb * fc) > 0)
            {
                value = "Vo Nghiem";
            }
            else if (fb == 0)
            {
                value = b.ToString();
            }
            else if (fc == 0)
            {
                value = c.ToString();
            }
            if (fb * fc < 0)
            {
                do
                {
                    x = ((b + c) / 2);
                    fx = a6 * x * x * x * x * x * x + a5 * x * x * x * x * x + a4 * x * x * x * x + a3 * x * x * x + a2 * x * x + a1 * x + a;
                    if ((fx * fb) < 0)
                    {
                        c = x;
                    }

                    if ((fx * fc) < 0)
                    {
                        b = x;
                    }

                    fb = a6 * b * b * b * b * b * b + a5 * b * b * b * b * b + a4 * b * b * b * b + a3 * b * b * b + a2 * b * b + a1 * b + a;
                    fc = a6 * c * c * c * c * c * c + a5 * c * c * c * c * c + a4 * c * c * c * c + a3 * c * c * c + a2 * c * c + a1 * c + a;
                }
                while ((c - b) >= s);

                value = x.ToString();
            }
            return value;

        }
        //connectdata(@"Data Source = " + sqConnectionString + ";Version=3;");
        private void connectdata()
        {
            conn = new SQLiteConnection(@"Data Source = DB_DuLieu.sqlite;Version=3;");
        }
        /**
         * Bảng banghoinh3 ----------------------------------------------------------------------------------------------------------------
         **/
        // Kiểm tra có nhiệt độ xác định trong DB
        public Boolean checkBanghoinh3(double value)
        {
            connectdata();
            conn.Open();
            SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM banghoinh3 WHERE nhietdo = " + value, conn);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }
        // Lấy ra Dòng có nhiệt độ xác định trong DB
        public DataTable getBanghoinh3(double value)
        {
            connectdata();
            conn.Open();
            SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM banghoinh3 WHERE nhietdo = " + value, conn);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        // Lấy ra Dòng có nhiệt độ xác định trong DB
        public DataTable getBanghoinh3_noisuy_xd(double value)
        {
            connectdata();
            conn.Open();
            double a = value - 1;
            SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM banghoinh3 WHERE nhietdo between " + a + " and " + value, conn);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        // Lấy ra Dòng có nhiệt độ không xác định trong DB
        public DataTable getBanghoinh3_noisuy_kxd(double value)
        {
            connectdata();
            conn.Open();
            double a = value - 1;
            double b = value + 1;
            SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM banghoinh3 WHERE nhietdo between " + a + " and " + b, conn);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        /**
         * Bảng bangnh3 ----------------------------------------------------------------------------------------------------------------
        **/
        // Kiểm tra có nhiệt độ xác định trong DB
        public Boolean checkBangnh3(double value)
        {
            connectdata();
            conn.Open();
            SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM bangnh3 WHERE t = " + value, conn);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }
        // Lấy ra Dòng có nhiệt độ xác định trong DB
        public DataTable getBangnh3(double value)
        {
            connectdata();
            conn.Open();
            SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM bangnh3 WHERE t = " + value, conn);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        // Lấy ra Dòng có nhiệt độ xác định trong DB
        public DataTable getBangnh3_noisuy_xd(double value)
        {
            connectdata();
            conn.Open();
            double a = value - 10;
            SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM bangnh3 WHERE t between " + a + " and " + value, conn);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        // Lấy ra Dòng có nhiệt độ không xác định trong DB
        public DataTable getBangnh3_noisuy_kxd(double value)
        {
            connectdata();
            conn.Open();
            double a = value - 10;
            double b = value + 10;
            SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM bangnh3 WHERE t between " + a + " and " + b, conn);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        /**
        * Bảng bangh2o ----------------------------------------------------------------------------------------------------------------
       **/
        // Kiểm tra có nhiệt độ xác định trong DB
        public Boolean checkBangh2o(double value)
        {
            connectdata();
            conn.Open();
            SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM bangnh3 WHERE t = " + value, conn);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }
        // Lấy ra Dòng có nhiệt độ xác định trong DB
        public DataTable getBangh2o(double value)
        {
            connectdata();
            conn.Open();
            SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM bangh2o WHERE t = " + value, conn);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        // Lấy ra Dòng có nhiệt độ xác định trong DB
        public DataTable getBangh2o_noisuy_xd(double value)
        {
            connectdata();
            conn.Open();
            double a = value - 10;
            SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM bangnh3 WHERE t between " + a + " and " + value, conn);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        // Lấy ra Dòng có nhiệt độ không xác định trong DB
        public DataTable getBangh2o_noisuy_kxd(double value)
        {
            connectdata();
            conn.Open();
            double a = value + 1;
            SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM bangh2o WHERE p between " + value + " and " + a + " ORDER BY ID LIMIT 2", conn);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
    }
}
