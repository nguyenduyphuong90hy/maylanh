﻿/*
 * Created by SharpDevelop.
 * User: pokemon
 * Date: 05/07/2017
 * Time: 10:57 CH
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace ProjectX
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TabControl t1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.Label dn15;
		private System.Windows.Forms.Label dn14;
		private System.Windows.Forms.Label dn13;
		private System.Windows.Forms.Label dn12;
		private System.Windows.Forms.Label dn11;
		private System.Windows.Forms.Label dn10;
		private System.Windows.Forms.Label dn9;
		private System.Windows.Forms.Label dn8;
		private System.Windows.Forms.Label dn7;
		private System.Windows.Forms.Label dn6;
		private System.Windows.Forms.Label dn5;
		private System.Windows.Forms.Label dn4;
		private System.Windows.Forms.Label dn3;
		private System.Windows.Forms.Label dn2;
		private System.Windows.Forms.Label dn1;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBox5;
		private System.Windows.Forms.TextBox textBox4;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button thoat;
		private System.Windows.Forms.Button btn_tieptheo;
		private System.Windows.Forms.Button bnt_xuatdl;
		private System.Windows.Forms.Button btn_quaylai;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.label1 = new System.Windows.Forms.Label();
            this.t1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.thoat = new System.Windows.Forms.Button();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btn_danhgiaht = new System.Windows.Forms.Button();
            this.btn_tieptheo = new System.Windows.Forms.Button();
            this.bnt_xuatdl = new System.Windows.Forms.Button();
            this.btn_quaylai = new System.Windows.Forms.Button();
            this.dn15 = new System.Windows.Forms.Label();
            this.dn14 = new System.Windows.Forms.Label();
            this.dn13 = new System.Windows.Forms.Label();
            this.dn12 = new System.Windows.Forms.Label();
            this.dn11 = new System.Windows.Forms.Label();
            this.dn10 = new System.Windows.Forms.Label();
            this.dn9 = new System.Windows.Forms.Label();
            this.dn8 = new System.Windows.Forms.Label();
            this.dn7 = new System.Windows.Forms.Label();
            this.dn6 = new System.Windows.Forms.Label();
            this.dn5 = new System.Windows.Forms.Label();
            this.dn4 = new System.Windows.Forms.Label();
            this.dn3 = new System.Windows.Forms.Label();
            this.dn2 = new System.Windows.Forms.Label();
            this.dn1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btn_thoat1 = new System.Windows.Forms.Button();
            this.btn_xdll3 = new System.Windows.Forms.Button();
            this.btn_quaylai1 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tinhchonbom = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.txt_cs_t3_1 = new System.Windows.Forms.TextBox();
            this.txt_ns_t3_1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.thietbingungtu = new System.Windows.Forms.TabPage();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.thietbibayhoi = new System.Windows.Forms.TabPage();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label37 = new System.Windows.Forms.Label();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.binhhapthu = new System.Windows.Forms.TabPage();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.tohop = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label71 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label67 = new System.Windows.Forms.Label();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label95 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.button16 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label50 = new System.Windows.Forms.Label();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.t1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tinhchonbom.SuspendLayout();
            this.thietbingungtu.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.thietbibayhoi.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.binhhapthu.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tohop.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(129, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(699, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "PHẦN MỀM TÍNH TOÁN MÔ PHỎNG MÁY LẠNH HẤP THỤ MỘT CẤP";
            // 
            // t1
            // 
            this.t1.Controls.Add(this.tabPage1);
            this.t1.Controls.Add(this.tabPage2);
            this.t1.Controls.Add(this.tabPage3);
            this.t1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.t1.ItemSize = new System.Drawing.Size(150, 30);
            this.t1.Location = new System.Drawing.Point(0, 35);
            this.t1.Name = "t1";
            this.t1.SelectedIndex = 0;
            this.t1.Size = new System.Drawing.Size(828, 620);
            this.t1.TabIndex = 1;
            this.t1.Click += new System.EventHandler(this.t1_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.thoat);
            this.tabPage1.Controls.Add(this.textBox5);
            this.tabPage1.Controls.Add(this.textBox4);
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 34);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(820, 582);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "THÔNG SỐ ĐẦU VÀO";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(449, 501);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 38);
            this.button1.TabIndex = 17;
            this.button1.Text = "Tiếp Theo";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // thoat
            // 
            this.thoat.Location = new System.Drawing.Point(259, 501);
            this.thoat.Name = "thoat";
            this.thoat.Size = new System.Drawing.Size(120, 38);
            this.thoat.TabIndex = 16;
            this.thoat.Text = "Thoát";
            this.thoat.UseVisualStyleBackColor = true;
            this.thoat.Click += new System.EventHandler(this.thoat_Click);
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(422, 134);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(129, 22);
            this.textBox5.TabIndex = 12;
            this.textBox5.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextBox5KeyUp);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(422, 87);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(129, 22);
            this.textBox4.TabIndex = 11;
            this.textBox4.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextBox4KeyUp);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(422, 39);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(129, 22);
            this.textBox1.TabIndex = 8;
            this.textBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextBox1KeyUp);
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(255, 23);
            this.label6.TabIndex = 4;
            this.label6.Text = "Nhiệt độ bay hơi tại dàn bay hơi";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(410, 35);
            this.label5.TabIndex = 3;
            this.label5.Text = "Nhiệt độ ngưng tụ tại dàn ngưng và bình hấp thụ";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(203, 23);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nhiệt độ nước gia nhiệt vào";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btn_danhgiaht);
            this.tabPage2.Controls.Add(this.btn_tieptheo);
            this.tabPage2.Controls.Add(this.bnt_xuatdl);
            this.tabPage2.Controls.Add(this.btn_quaylai);
            this.tabPage2.Controls.Add(this.dn15);
            this.tabPage2.Controls.Add(this.dn14);
            this.tabPage2.Controls.Add(this.dn13);
            this.tabPage2.Controls.Add(this.dn12);
            this.tabPage2.Controls.Add(this.dn11);
            this.tabPage2.Controls.Add(this.dn10);
            this.tabPage2.Controls.Add(this.dn9);
            this.tabPage2.Controls.Add(this.dn8);
            this.tabPage2.Controls.Add(this.dn7);
            this.tabPage2.Controls.Add(this.dn6);
            this.tabPage2.Controls.Add(this.dn5);
            this.tabPage2.Controls.Add(this.dn4);
            this.tabPage2.Controls.Add(this.dn3);
            this.tabPage2.Controls.Add(this.dn2);
            this.tabPage2.Controls.Add(this.dn1);
            this.tabPage2.Controls.Add(this.pictureBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 34);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(820, 582);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "MÔ PHỎNG ĐIỂM NÚT CÁC QUÁ TRÌNH";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btn_danhgiaht
            // 
            this.btn_danhgiaht.Location = new System.Drawing.Point(365, 543);
            this.btn_danhgiaht.Name = "btn_danhgiaht";
            this.btn_danhgiaht.Size = new System.Drawing.Size(75, 25);
            this.btn_danhgiaht.TabIndex = 19;
            this.btn_danhgiaht.Text = "Đánh giá HT";
            this.btn_danhgiaht.UseVisualStyleBackColor = true;
            this.btn_danhgiaht.Click += new System.EventHandler(this.btn_danhgiaht_Click);
            // 
            // btn_tieptheo
            // 
            this.btn_tieptheo.Location = new System.Drawing.Point(590, 543);
            this.btn_tieptheo.Name = "btn_tieptheo";
            this.btn_tieptheo.Size = new System.Drawing.Size(75, 25);
            this.btn_tieptheo.TabIndex = 18;
            this.btn_tieptheo.Text = "Tiếp theo";
            this.btn_tieptheo.UseVisualStyleBackColor = true;
            this.btn_tieptheo.Click += new System.EventHandler(this.Btn_tieptheoClick);
            // 
            // bnt_xuatdl
            // 
            this.bnt_xuatdl.Location = new System.Drawing.Point(457, 543);
            this.bnt_xuatdl.Name = "bnt_xuatdl";
            this.bnt_xuatdl.Size = new System.Drawing.Size(113, 25);
            this.bnt_xuatdl.TabIndex = 17;
            this.bnt_xuatdl.Text = "Xuất dữ liệu";
            this.bnt_xuatdl.UseVisualStyleBackColor = true;
            // 
            // btn_quaylai
            // 
            this.btn_quaylai.Location = new System.Drawing.Point(273, 543);
            this.btn_quaylai.Name = "btn_quaylai";
            this.btn_quaylai.Size = new System.Drawing.Size(75, 25);
            this.btn_quaylai.TabIndex = 16;
            this.btn_quaylai.Text = "Quay lại";
            this.btn_quaylai.UseVisualStyleBackColor = true;
            this.btn_quaylai.Click += new System.EventHandler(this.Btn_quaylaiClick);
            // 
            // dn15
            // 
            this.dn15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dn15.Location = new System.Drawing.Point(591, 144);
            this.dn15.Name = "dn15";
            this.dn15.Size = new System.Drawing.Size(24, 21);
            this.dn15.TabIndex = 15;
            this.dn15.Text = "15";
            this.dn15.MouseHover += new System.EventHandler(this.dn15_MouseHover);
            // 
            // dn14
            // 
            this.dn14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dn14.Location = new System.Drawing.Point(665, 149);
            this.dn14.Name = "dn14";
            this.dn14.Size = new System.Drawing.Size(24, 21);
            this.dn14.TabIndex = 14;
            this.dn14.Text = "14";
            this.dn14.MouseHover += new System.EventHandler(this.dn14_MouseHover);
            // 
            // dn13
            // 
            this.dn13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dn13.Location = new System.Drawing.Point(574, 168);
            this.dn13.Name = "dn13";
            this.dn13.Size = new System.Drawing.Size(24, 21);
            this.dn13.TabIndex = 13;
            this.dn13.Text = "13";
            this.dn13.MouseHover += new System.EventHandler(this.dn13_MouseHover);
            // 
            // dn12
            // 
            this.dn12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dn12.Location = new System.Drawing.Point(573, 113);
            this.dn12.Name = "dn12";
            this.dn12.Size = new System.Drawing.Size(24, 21);
            this.dn12.TabIndex = 12;
            this.dn12.Text = "12";
            this.dn12.MouseHover += new System.EventHandler(this.dn12_MouseHover);
            // 
            // dn11
            // 
            this.dn11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dn11.Location = new System.Drawing.Point(575, 68);
            this.dn11.Name = "dn11";
            this.dn11.Size = new System.Drawing.Size(24, 21);
            this.dn11.TabIndex = 11;
            this.dn11.Text = "11";
            this.dn11.MouseHover += new System.EventHandler(this.dn11_MouseHover);
            // 
            // dn10
            // 
            this.dn10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dn10.Location = new System.Drawing.Point(524, 476);
            this.dn10.Name = "dn10";
            this.dn10.Size = new System.Drawing.Size(24, 21);
            this.dn10.TabIndex = 10;
            this.dn10.Text = "10";
            this.dn10.MouseHover += new System.EventHandler(this.dn10_MouseHover);
            // 
            // dn9
            // 
            this.dn9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dn9.Location = new System.Drawing.Point(612, 385);
            this.dn9.Name = "dn9";
            this.dn9.Size = new System.Drawing.Size(23, 21);
            this.dn9.TabIndex = 9;
            this.dn9.Text = "9";
            this.dn9.MouseHover += new System.EventHandler(this.dn9_MouseHover);
            // 
            // dn8
            // 
            this.dn8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dn8.Location = new System.Drawing.Point(529, 315);
            this.dn8.Name = "dn8";
            this.dn8.Size = new System.Drawing.Size(22, 21);
            this.dn8.TabIndex = 8;
            this.dn8.Text = "8";
            this.dn8.MouseHover += new System.EventHandler(this.dn8_MouseHover);
            // 
            // dn7
            // 
            this.dn7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dn7.Location = new System.Drawing.Point(574, 268);
            this.dn7.Name = "dn7";
            this.dn7.Size = new System.Drawing.Size(23, 21);
            this.dn7.TabIndex = 7;
            this.dn7.Text = "7";
            this.dn7.MouseHover += new System.EventHandler(this.dn7_MouseHover);
            // 
            // dn6
            // 
            this.dn6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dn6.Location = new System.Drawing.Point(153, 205);
            this.dn6.Name = "dn6";
            this.dn6.Size = new System.Drawing.Size(22, 21);
            this.dn6.TabIndex = 6;
            this.dn6.Text = "6";
            this.dn6.MouseHover += new System.EventHandler(this.dn6_MouseHover);
            // 
            // dn5
            // 
            this.dn5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dn5.Location = new System.Drawing.Point(117, 309);
            this.dn5.Name = "dn5";
            this.dn5.Size = new System.Drawing.Size(23, 21);
            this.dn5.TabIndex = 5;
            this.dn5.Text = "5";
            this.dn5.MouseHover += new System.EventHandler(this.dn5_MouseHover);
            // 
            // dn4
            // 
            this.dn4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dn4.Location = new System.Drawing.Point(58, 423);
            this.dn4.Name = "dn4";
            this.dn4.Size = new System.Drawing.Size(22, 21);
            this.dn4.TabIndex = 4;
            this.dn4.Text = "4";
            this.dn4.MouseHover += new System.EventHandler(this.dn4_MouseHover);
            // 
            // dn3
            // 
            this.dn3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dn3.Location = new System.Drawing.Point(61, 309);
            this.dn3.Name = "dn3";
            this.dn3.Size = new System.Drawing.Size(22, 21);
            this.dn3.TabIndex = 3;
            this.dn3.Text = "3";
            this.dn3.MouseHover += new System.EventHandler(this.dn3_MouseHover);
            // 
            // dn2
            // 
            this.dn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dn2.Location = new System.Drawing.Point(59, 142);
            this.dn2.Name = "dn2";
            this.dn2.Size = new System.Drawing.Size(22, 21);
            this.dn2.TabIndex = 2;
            this.dn2.Text = "2";
            this.dn2.MouseHover += new System.EventHandler(this.Dn2MouseHover);
            // 
            // dn1
            // 
            this.dn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dn1.Location = new System.Drawing.Point(235, 52);
            this.dn1.Name = "dn1";
            this.dn1.Size = new System.Drawing.Size(19, 21);
            this.dn1.TabIndex = 1;
            this.dn1.Text = "1";
            this.dn1.MouseHover += new System.EventHandler(this.Dn1MouseHover);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(8, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(802, 505);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btn_thoat1);
            this.tabPage3.Controls.Add(this.btn_xdll3);
            this.tabPage3.Controls.Add(this.btn_quaylai1);
            this.tabPage3.Controls.Add(this.tabControl1);
            this.tabPage3.Location = new System.Drawing.Point(4, 34);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(820, 582);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "TÍNH TOÁN THIẾT KẾ CHẾ TẠO MÔ HÌNH";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btn_thoat1
            // 
            this.btn_thoat1.Location = new System.Drawing.Point(495, 540);
            this.btn_thoat1.Name = "btn_thoat1";
            this.btn_thoat1.Size = new System.Drawing.Size(101, 39);
            this.btn_thoat1.TabIndex = 3;
            this.btn_thoat1.Text = "Thoát";
            this.btn_thoat1.UseVisualStyleBackColor = true;
            this.btn_thoat1.Click += new System.EventHandler(this.btn_thoat1_Click);
            // 
            // btn_xdll3
            // 
            this.btn_xdll3.Location = new System.Drawing.Point(351, 540);
            this.btn_xdll3.Name = "btn_xdll3";
            this.btn_xdll3.Size = new System.Drawing.Size(121, 39);
            this.btn_xdll3.TabIndex = 2;
            this.btn_xdll3.Text = "Xuất Dữ Liệu";
            this.btn_xdll3.UseVisualStyleBackColor = true;
            // 
            // btn_quaylai1
            // 
            this.btn_quaylai1.Location = new System.Drawing.Point(235, 539);
            this.btn_quaylai1.Name = "btn_quaylai1";
            this.btn_quaylai1.Size = new System.Drawing.Size(90, 40);
            this.btn_quaylai1.TabIndex = 1;
            this.btn_quaylai1.Text = "Quay Lại";
            this.btn_quaylai1.UseVisualStyleBackColor = true;
            this.btn_quaylai1.Click += new System.EventHandler(this.btn_quaylai1_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tinhchonbom);
            this.tabControl1.Controls.Add(this.thietbingungtu);
            this.tabControl1.Controls.Add(this.thietbibayhoi);
            this.tabControl1.Controls.Add(this.binhhapthu);
            this.tabControl1.Controls.Add(this.tohop);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(814, 531);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl1_Selected);
            // 
            // tinhchonbom
            // 
            this.tinhchonbom.Controls.Add(this.label8);
            this.tinhchonbom.Controls.Add(this.label7);
            this.tinhchonbom.Controls.Add(this.button4);
            this.tinhchonbom.Controls.Add(this.button3);
            this.tinhchonbom.Controls.Add(this.txt_cs_t3_1);
            this.tinhchonbom.Controls.Add(this.txt_ns_t3_1);
            this.tinhchonbom.Controls.Add(this.label4);
            this.tinhchonbom.Controls.Add(this.label3);
            this.tinhchonbom.Location = new System.Drawing.Point(4, 24);
            this.tinhchonbom.Name = "tinhchonbom";
            this.tinhchonbom.Padding = new System.Windows.Forms.Padding(3);
            this.tinhchonbom.Size = new System.Drawing.Size(806, 503);
            this.tinhchonbom.TabIndex = 0;
            this.tinhchonbom.Text = "Tính Chọn Bơm";
            this.tinhchonbom.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(556, 139);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 18);
            this.label8.TabIndex = 8;
            this.label8.Text = "kW";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(556, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 18);
            this.label7.TabIndex = 7;
            this.label7.Text = "kW";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(443, 215);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(107, 33);
            this.button4.TabIndex = 6;
            this.button4.Text = "Tiếp Tục";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(278, 215);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(113, 33);
            this.button3.TabIndex = 5;
            this.button3.Text = "Tính Toán";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // txt_cs_t3_1
            // 
            this.txt_cs_t3_1.Location = new System.Drawing.Point(386, 135);
            this.txt_cs_t3_1.Name = "txt_cs_t3_1";
            this.txt_cs_t3_1.ReadOnly = true;
            this.txt_cs_t3_1.Size = new System.Drawing.Size(164, 21);
            this.txt_cs_t3_1.TabIndex = 3;
            // 
            // txt_ns_t3_1
            // 
            this.txt_ns_t3_1.Location = new System.Drawing.Point(386, 78);
            this.txt_ns_t3_1.Name = "txt_ns_t3_1";
            this.txt_ns_t3_1.Size = new System.Drawing.Size(164, 21);
            this.txt_ns_t3_1.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(39, 135);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(191, 24);
            this.label4.TabIndex = 1;
            this.label4.Text = "Công suất của bơm";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(39, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(319, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "Năng suất làm lạnh của máy lạnh";
            // 
            // thietbingungtu
            // 
            this.thietbingungtu.Controls.Add(this.button6);
            this.thietbingungtu.Controls.Add(this.button5);
            this.thietbingungtu.Controls.Add(this.button2);
            this.thietbingungtu.Controls.Add(this.button7);
            this.thietbingungtu.Controls.Add(this.groupBox2);
            this.thietbingungtu.Controls.Add(this.groupBox1);
            this.thietbingungtu.Location = new System.Drawing.Point(4, 24);
            this.thietbingungtu.Name = "thietbingungtu";
            this.thietbingungtu.Padding = new System.Windows.Forms.Padding(3);
            this.thietbingungtu.Size = new System.Drawing.Size(806, 503);
            this.thietbingungtu.TabIndex = 1;
            this.thietbingungtu.Text = "Thiết Bị Ngưng Tụ";
            this.thietbingungtu.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(569, 446);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(101, 39);
            this.button6.TabIndex = 4;
            this.button6.Text = "Thoát";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(439, 446);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(90, 40);
            this.button5.TabIndex = 6;
            this.button5.Text = "Tiếp Tục";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(302, 446);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(90, 40);
            this.button2.TabIndex = 5;
            this.button2.Text = "Tính Toán";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(179, 446);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(90, 40);
            this.button7.TabIndex = 4;
            this.button7.Text = "Quay Lại";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.richTextBox1);
            this.groupBox2.Controls.Add(this.label65);
            this.groupBox2.Controls.Add(this.label36);
            this.groupBox2.Controls.Add(this.label35);
            this.groupBox2.Controls.Add(this.label34);
            this.groupBox2.Controls.Add(this.label33);
            this.groupBox2.Controls.Add(this.textBox17);
            this.groupBox2.Controls.Add(this.textBox16);
            this.groupBox2.Controls.Add(this.textBox15);
            this.groupBox2.Controls.Add(this.textBox14);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Location = new System.Drawing.Point(404, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(396, 410);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.Location = new System.Drawing.Point(9, 323);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(381, 72);
            this.richTextBox1.TabIndex = 42;
            this.richTextBox1.Text = "Thiết bị bay hơi tính toán là dàn bay hơi kiểu ống xoán có cánh. Không khí lưu độ" +
    "ng ngoài chùm ống và môi chất sôi trong ống";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(6, 297);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(59, 16);
            this.label65.TabIndex = 41;
            this.label65.Text = "Ghi chú";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(338, 32);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(28, 16);
            this.label36.TabIndex = 40;
            this.label36.Text = "m2";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(338, 101);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(34, 16);
            this.label35.TabIndex = 39;
            this.label35.Text = "ống";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(338, 135);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(34, 16);
            this.label34.TabIndex = 34;
            this.label34.Text = "ống";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(338, 64);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(20, 16);
            this.label33.TabIndex = 38;
            this.label33.Text = "m";
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(220, 132);
            this.textBox17.Name = "textBox17";
            this.textBox17.ReadOnly = true;
            this.textBox17.Size = new System.Drawing.Size(112, 21);
            this.textBox17.TabIndex = 37;
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(220, 94);
            this.textBox16.Name = "textBox16";
            this.textBox16.ReadOnly = true;
            this.textBox16.Size = new System.Drawing.Size(112, 21);
            this.textBox16.TabIndex = 36;
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(220, 58);
            this.textBox15.Name = "textBox15";
            this.textBox15.ReadOnly = true;
            this.textBox15.Size = new System.Drawing.Size(112, 21);
            this.textBox15.TabIndex = 35;
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(220, 24);
            this.textBox14.Name = "textBox14";
            this.textBox14.ReadOnly = true;
            this.textBox14.Size = new System.Drawing.Size(112, 21);
            this.textBox14.TabIndex = 34;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(4, 137);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(192, 16);
            this.label22.TabIndex = 4;
            this.label22.Text = "Tổng số ống trên một hàng";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(4, 99);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(95, 16);
            this.label21.TabIndex = 3;
            this.label21.Text = "Tổng số ống";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(4, 61);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(141, 16);
            this.label20.TabIndex = 2;
            this.label20.Text = "Tổng chiều dài ống";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(4, 27);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(213, 16);
            this.label19.TabIndex = 1;
            this.label19.Text = "Diện tích bề mặt trao đổi nhiệt";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Controls.Add(this.label31);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.textBox13);
            this.groupBox1.Controls.Add(this.textBox12);
            this.groupBox1.Controls.Add(this.textBox11);
            this.groupBox1.Controls.Add(this.textBox10);
            this.groupBox1.Controls.Add(this.textBox9);
            this.groupBox1.Controls.Add(this.textBox8);
            this.groupBox1.Controls.Add(this.textBox7);
            this.groupBox1.Controls.Add(this.textBox6);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(392, 410);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(330, 253);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(20, 16);
            this.label29.TabIndex = 33;
            this.label29.Text = "m";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(330, 213);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(20, 16);
            this.label28.TabIndex = 32;
            this.label28.Text = "m";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(330, 174);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(20, 16);
            this.label27.TabIndex = 31;
            this.label27.Text = "m";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(330, 142);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(20, 16);
            this.label26.TabIndex = 30;
            this.label26.Text = "m";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(330, 381);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(60, 16);
            this.label32.TabIndex = 29;
            this.label32.Text = "W/m2.K";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(330, 336);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(42, 16);
            this.label31.TabIndex = 28;
            this.label31.Text = "hàng";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(330, 295);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(33, 16);
            this.label30.TabIndex = 27;
            this.label30.Text = "m/s";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(330, 101);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(20, 16);
            this.label25.TabIndex = 22;
            this.label25.Text = "m";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(330, 66);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(27, 16);
            this.label24.TabIndex = 21;
            this.label24.Text = "oC";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(330, 30);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(27, 16);
            this.label23.TabIndex = 20;
            this.label23.Text = "oC";
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(197, 376);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(127, 21);
            this.textBox13.TabIndex = 19;
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(197, 333);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(127, 21);
            this.textBox12.TabIndex = 18;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(197, 290);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(127, 21);
            this.textBox11.TabIndex = 17;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(197, 248);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(127, 21);
            this.textBox10.TabIndex = 16;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(197, 208);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(127, 21);
            this.textBox9.TabIndex = 15;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(197, 169);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(127, 21);
            this.textBox8.TabIndex = 14;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(197, 137);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(127, 21);
            this.textBox7.TabIndex = 13;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(197, 96);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(127, 21);
            this.textBox6.TabIndex = 12;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(197, 61);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(127, 21);
            this.textBox3.TabIndex = 11;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(197, 27);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(127, 21);
            this.textBox2.TabIndex = 10;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(6, 379);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(90, 16);
            this.label18.TabIndex = 9;
            this.label18.Text = "Vật liệu ống";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(6, 211);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(120, 16);
            this.label17.TabIndex = 8;
            this.label17.Text = "Bước ống ngang";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 253);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(112, 16);
            this.label16.TabIndex = 7;
            this.label16.Text = "Bướng ống dọc";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(6, 295);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(133, 16);
            this.label15.TabIndex = 6;
            this.label15.Text = "Tốc độ dòng chảy";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(6, 336);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(95, 16);
            this.label14.TabIndex = 5;
            this.label14.Text = "Số hàng ống";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(6, 174);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(132, 16);
            this.label13.TabIndex = 4;
            this.label13.Text = "Chiều dài một ống";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 61);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(121, 16);
            this.label12.TabIndex = 3;
            this.label12.Text = "Nhiệt độ nước ra";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 99);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(182, 16);
            this.label11.TabIndex = 2;
            this.label11.Text = "Đường kính trong của ống";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 137);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(186, 16);
            this.label10.TabIndex = 1;
            this.label10.Text = "Đường kính ngoài của ống";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(142, 16);
            this.label9.TabIndex = 0;
            this.label9.Text = "Nhiệt độ ngước vào";
            // 
            // thietbibayhoi
            // 
            this.thietbibayhoi.Controls.Add(this.button8);
            this.thietbibayhoi.Controls.Add(this.button9);
            this.thietbibayhoi.Controls.Add(this.button10);
            this.thietbibayhoi.Controls.Add(this.button11);
            this.thietbibayhoi.Controls.Add(this.richTextBox2);
            this.thietbibayhoi.Controls.Add(this.label66);
            this.thietbibayhoi.Controls.Add(this.groupBox3);
            this.thietbibayhoi.Controls.Add(this.groupBox4);
            this.thietbibayhoi.Location = new System.Drawing.Point(4, 24);
            this.thietbibayhoi.Name = "thietbibayhoi";
            this.thietbibayhoi.Padding = new System.Windows.Forms.Padding(3);
            this.thietbibayhoi.Size = new System.Drawing.Size(806, 503);
            this.thietbibayhoi.TabIndex = 2;
            this.thietbibayhoi.Text = "Thiết Bị Bay Hơi";
            this.thietbibayhoi.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(549, 468);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(101, 28);
            this.button8.TabIndex = 45;
            this.button8.Text = "Thoát";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(419, 468);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(90, 29);
            this.button9.TabIndex = 48;
            this.button9.Text = "Tiếp Tục";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(282, 468);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(90, 29);
            this.button10.TabIndex = 47;
            this.button10.Text = "Tính Toán";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(159, 468);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(90, 29);
            this.button11.TabIndex = 46;
            this.button11.Text = "Quay Lại";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // richTextBox2
            // 
            this.richTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox2.Location = new System.Drawing.Point(426, 355);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(374, 72);
            this.richTextBox2.TabIndex = 44;
            this.richTextBox2.Text = "Thiết bị bay hơi tính toán là dàn bay hơi kiểu ống xoán có cánh. Không khí lưu độ" +
    "ng ngoài chùm ống và môi chất sôi trong ống";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(423, 329);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(59, 16);
            this.label66.TabIndex = 43;
            this.label66.Text = "Ghi chú";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label37);
            this.groupBox3.Controls.Add(this.textBox21);
            this.groupBox3.Controls.Add(this.label44);
            this.groupBox3.Location = new System.Drawing.Point(419, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(381, 424);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(347, 62);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(28, 16);
            this.label37.TabIndex = 40;
            this.label37.Text = "m2";
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(229, 59);
            this.textBox21.Name = "textBox21";
            this.textBox21.ReadOnly = true;
            this.textBox21.Size = new System.Drawing.Size(112, 21);
            this.textBox21.TabIndex = 34;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(4, 27);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(301, 16);
            this.label44.TabIndex = 1;
            this.label44.Text = "Diện tích bề mặt trao đổi nhiệt ống có cánh";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label49);
            this.groupBox4.Controls.Add(this.label38);
            this.groupBox4.Controls.Add(this.label43);
            this.groupBox4.Controls.Add(this.label42);
            this.groupBox4.Controls.Add(this.label40);
            this.groupBox4.Controls.Add(this.textBox19);
            this.groupBox4.Controls.Add(this.label41);
            this.groupBox4.Controls.Add(this.textBox18);
            this.groupBox4.Controls.Add(this.label39);
            this.groupBox4.Controls.Add(this.label45);
            this.groupBox4.Controls.Add(this.label46);
            this.groupBox4.Controls.Add(this.label47);
            this.groupBox4.Controls.Add(this.label48);
            this.groupBox4.Controls.Add(this.label52);
            this.groupBox4.Controls.Add(this.label53);
            this.groupBox4.Controls.Add(this.label54);
            this.groupBox4.Controls.Add(this.textBox22);
            this.groupBox4.Controls.Add(this.textBox23);
            this.groupBox4.Controls.Add(this.textBox24);
            this.groupBox4.Controls.Add(this.textBox25);
            this.groupBox4.Controls.Add(this.textBox26);
            this.groupBox4.Controls.Add(this.textBox27);
            this.groupBox4.Controls.Add(this.textBox28);
            this.groupBox4.Controls.Add(this.textBox29);
            this.groupBox4.Controls.Add(this.textBox30);
            this.groupBox4.Controls.Add(this.textBox31);
            this.groupBox4.Controls.Add(this.label55);
            this.groupBox4.Controls.Add(this.label56);
            this.groupBox4.Controls.Add(this.label57);
            this.groupBox4.Controls.Add(this.label58);
            this.groupBox4.Controls.Add(this.label59);
            this.groupBox4.Controls.Add(this.label60);
            this.groupBox4.Controls.Add(this.label61);
            this.groupBox4.Controls.Add(this.label62);
            this.groupBox4.Controls.Add(this.label63);
            this.groupBox4.Controls.Add(this.label64);
            this.groupBox4.Location = new System.Drawing.Point(6, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(392, 450);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(330, 385);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(20, 16);
            this.label49.TabIndex = 43;
            this.label49.Text = "m";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(330, 349);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(20, 16);
            this.label38.TabIndex = 42;
            this.label38.Text = "m";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(330, 314);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(20, 16);
            this.label43.TabIndex = 41;
            this.label43.Text = "m";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(330, 277);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(42, 16);
            this.label42.TabIndex = 40;
            this.label42.Text = "hàng";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(330, 428);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(60, 16);
            this.label40.TabIndex = 39;
            this.label40.Text = "W/m2.K";
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(197, 423);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(127, 21);
            this.textBox19.TabIndex = 38;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(6, 426);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(90, 16);
            this.label41.TabIndex = 37;
            this.label41.Text = "Vật liệu ống";
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(197, 382);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(127, 21);
            this.textBox18.TabIndex = 35;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(6, 385);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(114, 16);
            this.label39.TabIndex = 34;
            this.label39.Text = "Chiều dày cánh";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(330, 240);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(20, 16);
            this.label45.TabIndex = 33;
            this.label45.Text = "m";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(330, 203);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(20, 16);
            this.label46.TabIndex = 32;
            this.label46.Text = "m";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(330, 167);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(20, 16);
            this.label47.TabIndex = 31;
            this.label47.Text = "m";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(330, 131);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(20, 16);
            this.label48.TabIndex = 30;
            this.label48.Text = "m";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(330, 95);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(20, 16);
            this.label52.TabIndex = 22;
            this.label52.Text = "m";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(330, 59);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(27, 16);
            this.label53.TabIndex = 21;
            this.label53.Text = "oC";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(330, 23);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(27, 16);
            this.label54.TabIndex = 20;
            this.label54.Text = "oC";
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(197, 346);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(127, 21);
            this.textBox22.TabIndex = 19;
            // 
            // textBox23
            // 
            this.textBox23.Location = new System.Drawing.Point(197, 309);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(127, 21);
            this.textBox23.TabIndex = 18;
            // 
            // textBox24
            // 
            this.textBox24.Location = new System.Drawing.Point(197, 272);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(127, 21);
            this.textBox24.TabIndex = 17;
            // 
            // textBox25
            // 
            this.textBox25.Location = new System.Drawing.Point(197, 235);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(127, 21);
            this.textBox25.TabIndex = 16;
            // 
            // textBox26
            // 
            this.textBox26.Location = new System.Drawing.Point(197, 198);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(127, 21);
            this.textBox26.TabIndex = 15;
            // 
            // textBox27
            // 
            this.textBox27.Location = new System.Drawing.Point(197, 162);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(127, 21);
            this.textBox27.TabIndex = 14;
            // 
            // textBox28
            // 
            this.textBox28.Location = new System.Drawing.Point(197, 126);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(127, 21);
            this.textBox28.TabIndex = 13;
            // 
            // textBox29
            // 
            this.textBox29.Location = new System.Drawing.Point(197, 90);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(127, 21);
            this.textBox29.TabIndex = 12;
            // 
            // textBox30
            // 
            this.textBox30.Location = new System.Drawing.Point(197, 54);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(127, 21);
            this.textBox30.TabIndex = 11;
            // 
            // textBox31
            // 
            this.textBox31.Location = new System.Drawing.Point(197, 20);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(127, 21);
            this.textBox31.TabIndex = 10;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(6, 349);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(114, 16);
            this.label55.TabIndex = 9;
            this.label55.Text = "Chiều cao cánh";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(6, 201);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(120, 16);
            this.label56.TabIndex = 8;
            this.label56.Text = "Bước ống ngang";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(6, 240);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(112, 16);
            this.label57.TabIndex = 7;
            this.label57.Text = "Bướng ống dọc";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(6, 277);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(95, 16);
            this.label58.TabIndex = 6;
            this.label58.Text = "Số hàng ống";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(6, 312);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(80, 16);
            this.label59.TabIndex = 5;
            this.label59.Text = "Bước cánh";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(6, 167);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(132, 16);
            this.label60.TabIndex = 4;
            this.label60.Text = "Chiều dài một ống";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(6, 54);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(154, 16);
            this.label61.TabIndex = 3;
            this.label61.Text = "Nhiệt độ không khí ra";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(6, 93);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(182, 16);
            this.label62.TabIndex = 2;
            this.label62.Text = "Đường kính trong của ống";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(6, 126);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(186, 16);
            this.label63.TabIndex = 1;
            this.label63.Text = "Đường kính ngoài của ống";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(6, 20);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(166, 16);
            this.label64.TabIndex = 0;
            this.label64.Text = "Nhiệt độ không khí vào";
            // 
            // binhhapthu
            // 
            this.binhhapthu.Controls.Add(this.button12);
            this.binhhapthu.Controls.Add(this.button13);
            this.binhhapthu.Controls.Add(this.button14);
            this.binhhapthu.Controls.Add(this.button15);
            this.binhhapthu.Controls.Add(this.groupBox5);
            this.binhhapthu.Controls.Add(this.groupBox6);
            this.binhhapthu.Location = new System.Drawing.Point(4, 24);
            this.binhhapthu.Name = "binhhapthu";
            this.binhhapthu.Padding = new System.Windows.Forms.Padding(3);
            this.binhhapthu.Size = new System.Drawing.Size(806, 503);
            this.binhhapthu.TabIndex = 3;
            this.binhhapthu.Text = "Bình Hấp Thụ";
            this.binhhapthu.UseVisualStyleBackColor = true;
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(549, 440);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(101, 39);
            this.button12.TabIndex = 9;
            this.button12.Text = "Thoát";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(419, 440);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(90, 40);
            this.button13.TabIndex = 12;
            this.button13.Text = "Tiếp Tục";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(282, 440);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(90, 40);
            this.button14.TabIndex = 11;
            this.button14.Text = "Tính Toán";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(159, 440);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(90, 40);
            this.button15.TabIndex = 10;
            this.button15.Text = "Quay Lại";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label51);
            this.groupBox5.Controls.Add(this.label69);
            this.groupBox5.Controls.Add(this.textBox33);
            this.groupBox5.Controls.Add(this.textBox34);
            this.groupBox5.Controls.Add(this.label72);
            this.groupBox5.Controls.Add(this.label73);
            this.groupBox5.Location = new System.Drawing.Point(404, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(396, 395);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(338, 51);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(28, 16);
            this.label51.TabIndex = 40;
            this.label51.Text = "m2";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(338, 103);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(20, 16);
            this.label69.TabIndex = 38;
            this.label69.Text = "m";
            // 
            // textBox33
            // 
            this.textBox33.Location = new System.Drawing.Point(220, 97);
            this.textBox33.Name = "textBox33";
            this.textBox33.ReadOnly = true;
            this.textBox33.Size = new System.Drawing.Size(112, 21);
            this.textBox33.TabIndex = 35;
            // 
            // textBox34
            // 
            this.textBox34.Location = new System.Drawing.Point(220, 46);
            this.textBox34.Name = "textBox34";
            this.textBox34.ReadOnly = true;
            this.textBox34.Size = new System.Drawing.Size(112, 21);
            this.textBox34.TabIndex = 34;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(4, 100);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(141, 16);
            this.label72.TabIndex = 2;
            this.label72.Text = "Tổng chiều dài ống";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(4, 27);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(301, 16);
            this.label73.TabIndex = 1;
            this.label73.Text = "Diện tích bề mặt trao đổi nhiệt ống có cánh";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label74);
            this.groupBox6.Controls.Add(this.label75);
            this.groupBox6.Controls.Add(this.label76);
            this.groupBox6.Controls.Add(this.label77);
            this.groupBox6.Controls.Add(this.label78);
            this.groupBox6.Controls.Add(this.label81);
            this.groupBox6.Controls.Add(this.textBox35);
            this.groupBox6.Controls.Add(this.textBox38);
            this.groupBox6.Controls.Add(this.textBox39);
            this.groupBox6.Controls.Add(this.textBox40);
            this.groupBox6.Controls.Add(this.textBox41);
            this.groupBox6.Controls.Add(this.textBox42);
            this.groupBox6.Controls.Add(this.label84);
            this.groupBox6.Controls.Add(this.label85);
            this.groupBox6.Controls.Add(this.label86);
            this.groupBox6.Controls.Add(this.label89);
            this.groupBox6.Controls.Add(this.label91);
            this.groupBox6.Controls.Add(this.label92);
            this.groupBox6.Location = new System.Drawing.Point(6, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(392, 395);
            this.groupBox6.TabIndex = 7;
            this.groupBox6.TabStop = false;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(331, 181);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(20, 16);
            this.label74.TabIndex = 33;
            this.label74.Text = "m";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(331, 141);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(20, 16);
            this.label75.TabIndex = 32;
            this.label75.Text = "m";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(331, 102);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(20, 16);
            this.label76.TabIndex = 31;
            this.label76.Text = "m";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(331, 70);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(20, 16);
            this.label77.TabIndex = 30;
            this.label77.Text = "m";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(331, 224);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(60, 16);
            this.label78.TabIndex = 29;
            this.label78.Text = "W/m2.K";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.Location = new System.Drawing.Point(331, 29);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(20, 16);
            this.label81.TabIndex = 22;
            this.label81.Text = "m";
            // 
            // textBox35
            // 
            this.textBox35.Location = new System.Drawing.Point(198, 219);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(127, 21);
            this.textBox35.TabIndex = 19;
            // 
            // textBox38
            // 
            this.textBox38.Location = new System.Drawing.Point(198, 176);
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(127, 21);
            this.textBox38.TabIndex = 16;
            // 
            // textBox39
            // 
            this.textBox39.Location = new System.Drawing.Point(198, 136);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(127, 21);
            this.textBox39.TabIndex = 15;
            // 
            // textBox40
            // 
            this.textBox40.Location = new System.Drawing.Point(198, 97);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(127, 21);
            this.textBox40.TabIndex = 14;
            // 
            // textBox41
            // 
            this.textBox41.Location = new System.Drawing.Point(198, 65);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(127, 21);
            this.textBox41.TabIndex = 13;
            // 
            // textBox42
            // 
            this.textBox42.Location = new System.Drawing.Point(198, 24);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(127, 21);
            this.textBox42.TabIndex = 12;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.Location = new System.Drawing.Point(7, 222);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(90, 16);
            this.label84.TabIndex = 9;
            this.label84.Text = "Vật liệu ống";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(7, 139);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(120, 16);
            this.label85.TabIndex = 8;
            this.label85.Text = "Bước ống ngang";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(7, 181);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(112, 16);
            this.label86.TabIndex = 7;
            this.label86.Text = "Bướng ống dọc";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(7, 102);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(132, 16);
            this.label89.TabIndex = 4;
            this.label89.Text = "Chiều dài một ống";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.Location = new System.Drawing.Point(7, 27);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(182, 16);
            this.label91.TabIndex = 2;
            this.label91.Text = "Đường kính trong của ống";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.Location = new System.Drawing.Point(7, 65);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(186, 16);
            this.label92.TabIndex = 1;
            this.label92.Text = "Đường kính ngoài của ống";
            // 
            // tohop
            // 
            this.tohop.Controls.Add(this.groupBox9);
            this.tohop.Controls.Add(this.groupBox11);
            this.tohop.Controls.Add(this.button16);
            this.tohop.Controls.Add(this.button18);
            this.tohop.Controls.Add(this.button19);
            this.tohop.Controls.Add(this.groupBox8);
            this.tohop.Location = new System.Drawing.Point(4, 24);
            this.tohop.Name = "tohop";
            this.tohop.Padding = new System.Windows.Forms.Padding(3);
            this.tohop.Size = new System.Drawing.Size(806, 503);
            this.tohop.TabIndex = 4;
            this.tohop.Text = "Tổ Hợp TBSH-TTL-TBNTHL";
            this.tohop.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.groupBox12);
            this.groupBox9.Controls.Add(this.groupBox10);
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.Location = new System.Drawing.Point(6, 215);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(794, 203);
            this.groupBox9.TabIndex = 11;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Tính chọn thiết bị ngưng tụ hồi lưu";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.label71);
            this.groupBox12.Controls.Add(this.label79);
            this.groupBox12.Controls.Add(this.label83);
            this.groupBox12.Controls.Add(this.label88);
            this.groupBox12.Controls.Add(this.label90);
            this.groupBox12.Controls.Add(this.label93);
            this.groupBox12.Controls.Add(this.textBox36);
            this.groupBox12.Controls.Add(this.textBox37);
            this.groupBox12.Controls.Add(this.textBox43);
            this.groupBox12.Location = new System.Drawing.Point(9, 33);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(378, 148);
            this.groupBox12.TabIndex = 42;
            this.groupBox12.TabStop = false;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(17, 25);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(182, 16);
            this.label71.TabIndex = 2;
            this.label71.Text = "Đường kính trong của ống";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(341, 100);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(20, 16);
            this.label79.TabIndex = 31;
            this.label79.Text = "m";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.Location = new System.Drawing.Point(17, 63);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(186, 16);
            this.label83.TabIndex = 1;
            this.label83.Text = "Đường kính ngoài của ống";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.Location = new System.Drawing.Point(341, 68);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(20, 16);
            this.label88.TabIndex = 30;
            this.label88.Text = "m";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.Location = new System.Drawing.Point(17, 100);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(132, 16);
            this.label90.TabIndex = 4;
            this.label90.Text = "Chiều dài một ống";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.Location = new System.Drawing.Point(341, 27);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(20, 16);
            this.label93.TabIndex = 22;
            this.label93.Text = "m";
            // 
            // textBox36
            // 
            this.textBox36.Location = new System.Drawing.Point(208, 22);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(127, 29);
            this.textBox36.TabIndex = 12;
            // 
            // textBox37
            // 
            this.textBox37.Location = new System.Drawing.Point(208, 95);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(127, 29);
            this.textBox37.TabIndex = 14;
            // 
            // textBox43
            // 
            this.textBox43.Location = new System.Drawing.Point(208, 63);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(127, 29);
            this.textBox43.TabIndex = 13;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label67);
            this.groupBox10.Controls.Add(this.textBox20);
            this.groupBox10.Controls.Add(this.label68);
            this.groupBox10.Location = new System.Drawing.Point(395, 29);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(390, 152);
            this.groupBox10.TabIndex = 10;
            this.groupBox10.TabStop = false;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(353, 27);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(28, 16);
            this.label67.TabIndex = 40;
            this.label67.Text = "m2";
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(222, 18);
            this.textBox20.Name = "textBox20";
            this.textBox20.ReadOnly = true;
            this.textBox20.Size = new System.Drawing.Size(125, 29);
            this.textBox20.TabIndex = 34;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(3, 31);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(213, 16);
            this.label68.TabIndex = 1;
            this.label68.Text = "Diện tích bề mặt trao đổi nhiệt";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label95);
            this.groupBox11.Controls.Add(this.label80);
            this.groupBox11.Controls.Add(this.label96);
            this.groupBox11.Controls.Add(this.label82);
            this.groupBox11.Controls.Add(this.label94);
            this.groupBox11.Controls.Add(this.label87);
            this.groupBox11.Controls.Add(this.textBox46);
            this.groupBox11.Controls.Add(this.textBox44);
            this.groupBox11.Controls.Add(this.textBox45);
            this.groupBox11.Location = new System.Drawing.Point(15, 39);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(378, 148);
            this.groupBox11.TabIndex = 41;
            this.groupBox11.TabStop = false;
            this.groupBox11.Enter += new System.EventHandler(this.groupBox11_Enter);
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.Location = new System.Drawing.Point(17, 25);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(182, 16);
            this.label95.TabIndex = 2;
            this.label95.Text = "Đường kính trong của ống";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(341, 100);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(20, 16);
            this.label80.TabIndex = 31;
            this.label80.Text = "m";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.Location = new System.Drawing.Point(17, 63);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(186, 16);
            this.label96.TabIndex = 1;
            this.label96.Text = "Đường kính ngoài của ống";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(341, 68);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(20, 16);
            this.label82.TabIndex = 30;
            this.label82.Text = "m";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.Location = new System.Drawing.Point(17, 100);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(132, 16);
            this.label94.TabIndex = 4;
            this.label94.Text = "Chiều dài một ống";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.Location = new System.Drawing.Point(341, 27);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(20, 16);
            this.label87.TabIndex = 22;
            this.label87.Text = "m";
            // 
            // textBox46
            // 
            this.textBox46.Location = new System.Drawing.Point(208, 22);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(127, 21);
            this.textBox46.TabIndex = 12;
            // 
            // textBox44
            // 
            this.textBox44.Location = new System.Drawing.Point(208, 95);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(127, 21);
            this.textBox44.TabIndex = 14;
            // 
            // textBox45
            // 
            this.textBox45.Location = new System.Drawing.Point(208, 63);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(127, 21);
            this.textBox45.TabIndex = 13;
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(459, 442);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(101, 39);
            this.button16.TabIndex = 43;
            this.button16.Text = "Thoát";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(329, 442);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(90, 40);
            this.button18.TabIndex = 45;
            this.button18.Text = "Tính Toán";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(194, 441);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(90, 40);
            this.button19.TabIndex = 44;
            this.button19.Text = "Quay Lại";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.groupBox7);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.Location = new System.Drawing.Point(6, 6);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(794, 203);
            this.groupBox8.TabIndex = 9;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Tính chọn bình sinh hơi";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label50);
            this.groupBox7.Controls.Add(this.textBox32);
            this.groupBox7.Controls.Add(this.label70);
            this.groupBox7.Location = new System.Drawing.Point(395, 29);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(390, 152);
            this.groupBox7.TabIndex = 10;
            this.groupBox7.TabStop = false;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(353, 27);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(28, 16);
            this.label50.TabIndex = 40;
            this.label50.Text = "m2";
            // 
            // textBox32
            // 
            this.textBox32.Location = new System.Drawing.Point(222, 18);
            this.textBox32.Name = "textBox32";
            this.textBox32.ReadOnly = true;
            this.textBox32.Size = new System.Drawing.Size(125, 29);
            this.textBox32.TabIndex = 34;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(3, 31);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(213, 16);
            this.label70.TabIndex = 1;
            this.label70.Text = "Diện tích bề mặt trao đổi nhiệt";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(826, 654);
            this.Controls.Add(this.t1);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Đại Học Bách Khoa Hà Nội";
            this.t1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tinhchonbom.ResumeLayout(false);
            this.tinhchonbom.PerformLayout();
            this.thietbingungtu.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.thietbibayhoi.ResumeLayout(false);
            this.thietbibayhoi.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.binhhapthu.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tohop.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);

		}

        private System.Windows.Forms.Button btn_danhgiaht;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tinhchonbom;
        private System.Windows.Forms.TabPage thietbingungtu;
        private System.Windows.Forms.Button btn_thoat1;
        private System.Windows.Forms.Button btn_xdll3;
        private System.Windows.Forms.Button btn_quaylai1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox txt_cs_t3_1;
        private System.Windows.Forms.TextBox txt_ns_t3_1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage thietbibayhoi;
        private System.Windows.Forms.TabPage binhhapthu;
        private System.Windows.Forms.TabPage tohop;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.TextBox textBox43;
    }
		}

