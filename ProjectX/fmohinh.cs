﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectX
{
    class fmohinh
    {
        /// <summary>
        /// Hệ số tuần hoàn của dung dịch
        /// </summary>
        public string I_f
        {
            set;
            get;
        }
        /// <summary>
        /// Năng suất lạnh riêng
        /// </summary>
        public string I_q0
        {
            set;
            get;
        }
        public string I_qk
        {
            set;
            get;
        }
        public string I_qA
        {
            set;
            get;

        }
        public string I_qhl
        {
            set;
            get;
        }
        public string I_qH
        {
            set;
            get;
        }
        public string I_qhnmc
        {
            set;
            get;
        }
        public string I_qhndd
        {
            set;
            get;
        }
        public string I_Z
        {
            set;
            get;
        }
        public string I_Q0
        {
            set;
            get;
        }
        public string I_md
        {
            set;
            get;
        }
        public string I_QK
        {
            set;
            get;
        }
        public string I_QA
        {
            set;
            get;
        }
        public string I_Qhl
        {
            set;
            get;
        }
        public string I_QH
        {
            set;
            get;
        }
        public string I_QHNmc
        {
            set;
            get;
        }
        public string I_QHNdd
        {
            set;
            get;
        }
        public string II_Nb
        {
            set;
            get;
        }
        public string III_Qk
        {
            set;
            get;
        }
        public string III_tk
        {
            set;
            get;
        }
        public string III_t1_2
        {
            set;
            get;
        }
        public string III_t2_2
        {
            set;
            get;
        }
        public string III_d1
        {
            set;
            get;
        }
        public string III_d2
        {
            set;
            get;
        }
        public string III_l
        {
            set;
            get;
        }
        public string III_s1
        {
            set;
            get;
        }
        public string III_s2
        {
            set;
            get;
        }
        public string III_w
        {
            set;
            get;
        }
        public string III_Yo
        {
            set;
            get;
        }
        public string III_Dt
        {
            set;
            get;
        }
        public string III_t2tb
        {
            set;
            get;
        }
        public string III_Y2
        {
            set;
            get;
        }
        public string III_Pr2
        {
            set;
            get;
        }
        public string III_V
        {
            set;
            get;
        }
        public string III_Pr2w
        {
            set;
            get;
        }
        public string III_Re2
        {
            set;
            get;
        }
        public string III_Nu2
        {
            set;
            get;
        }
        public string III_a2
        {
            set;
            get;
        }
        public string III_a1
        {
            set;
            get;
        }
        public string III_k
        {
            set;
            get;
        }
        public string III_F
        {
            set;
            get;
        }
        public string III_L
        {
            set;
            get;
        }
        public string III_n
        {
            set;
            get;
        }
        public string III_z
        {
            set;
            get;
        }
        public string III_m
        {
            set;
            get;
        }
        public string IV_Q0
        {
            set;
            get;
        }
        public string IV_t0
        {
            set;
            get;
        }
        public string IV_t1k
        {
            set;
            get;
        }
        public string IV_t2k
        {
            set;
            get;
        }
        public string IV_d1
        {
            set;
            get;
        }
        public string IV_d2
        {
            set;
            get;
        }
        public string IV_l
        {
            set;
            get;
        }
        public string IV_s1
        {
            set;
            get;
        }
        public string IV_s2
        {
            set;
            get;
        }
        public string IV_sc
        {
            set;
            get;
        }
        public string IV_Zc
        {
            set;
            get;
        }
        public string IV_h
        {
            set;
            get;
        }
        public string IV_Yo
        {
            set;
            get;
        }
        public string IV_DT
        {
            set;
            get;
        }
        public string IV_k
        {
            set;
            get;
        }
        public string IV_F
        {
            set;
            get;
        }
        public string V_QA
        {
            set;
            get;
        }
        public string V_t1_1
        {
            set;
            get;
        }
        public string V_t1_2
        {
            set;
            get;
        }
        public string V_t2_1
        {
            set;
            get;
        }
        public string V_t2_2
        {
            set;
            get;
        }
        public string V_d1
        {
            set;
            get;
        }
        public string V_d2
        {
            set;
            get;
        }
        public string V_l
        {
            set;
            get;
        }
        public string V_s1
        {
            set;
            get;
        }
        public string V_s2
        {
            set;
            get;
        }
        public string V_Yo
        {
            set;
            get;
        }
        public string V_k
        {
            set;
            get;
        }
        public string V_Dt
        {
            set;
            get;
        }
        public string V_F
        {
            set;
            get;
        }
        public string V_L
        {
            set;
            get;
        }
        public string VI_1_QH
        {
            set;
            get;
        }
        public string VI_1_t1_1
        {
            set;
            get;
        }
        public string VI_1_t1_2
        {
            set;
            get;
        }
        public string VI_1_t2_1
        {
            set;
            get;
        }
        public string VI_1_t2_2
        {
            set;
            get;
        }
        public string VI_1_d1
        {
            set;
            get;
        }
        public string VI_1_d2
        {
            set;
            get;
        }
        public string VI_1_Yo
        {
            set;
            get;
        }
        public string VI_2_Qhl
        {
            set;
            get;
        }
        public string VI_2_t1_1
        {
            set;
            get;
        }
        public string VI_2_t1_2
        {
            set;
            get;
        }
        public string VI_2_t2_1
        {
            set;
            get;
        }
        public string VI_2_t2_2
        {
            set;
            get;
        }
        public string VI_2_d1
        {
            set;
            get;
        }
        public string VI_2_d2
        {
            set;
            get;
        }
        public string VI_2_Yo
        {
            set;
            get;
        }
        public string VI_3_k
        {
            set;
            get;
        }
        public string VI_3_Dt
        {
            set;
            get;
        }
        public string VI_3_F
        {
            set;
            get;
        }
        public string VI_4_k
        {
            set;
            get;
        }
        public string VI_4_Dt
        {
            set;
            get;
        }
        public string VI_4_F
        {
            set;
            get;
        }
    }
   
}
