﻿/*
 * Created by SharpDevelop.
 * User: pokemon
 * Date: 05/07/2017
 * Time: 10:57 CH
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Data;
using System.Drawing;

namespace ProjectX
{
    /// <summary>
    /// Description of MainForm.
    /// </summary>
    public partial class MainForm : Form
    {

        fBien fbien = new fBien();
        ham classHam = new ham();
        //sql
        SQLiteConnection conn = new SQLiteConnection();
        SQLiteDataAdapter adapter = new SQLiteDataAdapter();
        float check;
        public MainForm()
        {
            // a, b hai biến cố định
            fbien.a = "-1219.64";
            fbien.b = "5.10";
            // nồng độ dung dịch đậm đặc
            fbien.Zd = "1";
            InitializeComponent();

            double ada = Math.Log(86);
            connectdata();
            conn.Open();
            textBox1.Text = "95";
            textBox4.Text = "30";
            textBox5.Text = "-12";

        }
        //Nhiệt độ nước gia nhiệt vào
        void TextBox1KeyUp(object sender, KeyEventArgs e)
        {
            try
            {

                fbien.tH1 = textBox1.Text.ToString();
                fbien.tH2 = (Convert.ToDouble(fbien.tH1) - 2).ToString();
                fbien.tH = (Convert.ToDouble(fbien.tH1) - 5).ToString();


            }
            catch (Exception)
            {

                ToolTip toolTipt1 = new ToolTip();
                toolTipt1.ShowAlways = true;
                toolTipt1.IsBalloon = true;
                toolTipt1.SetToolTip(textBox1, "Xin hãy nhập đúng dữ liệu");
            }

        }

        //Nhiệt độ ngưng tụ tại dàn ngưng và bình hấp thụ
        void TextBox4KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                fbien.tK = textBox4.Text.ToString();
                //fbien.Pk = (0.98067 * Math.Pow(10, (Convert.ToDouble(fbien.a) / (Convert.ToDouble(fbien.tK) + 273) + Convert.ToDouble(fbien.b)))).ToString();
                double Pk = 0.98067 * Math.Pow(10, (Convert.ToDouble(fbien.a) / (Convert.ToDouble(fbien.tK) + 273) + Convert.ToDouble(fbien.b)));
                fbien.Pk = PreciseDecimalValue(Pk, 3).ToString();

            }
            catch (Exception)
            {

                ToolTip toolTipt2 = new ToolTip();
                toolTipt2.ShowAlways = true;
                toolTipt2.IsBalloon = true;
                toolTipt2.SetToolTip(textBox4, "Xin hãy nhập đúng dữ liệu");
            }
        }

        //Nhiệt độ bay hơi tại dàn bay hơi
        void TextBox5KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                fbien.t0 = textBox5.Text.ToString();

                //fbien.P0 = (0.98067 * Math.Pow(10, ((Convert.ToDouble(fbien.a) / (Convert.ToDouble(fbien.t0) + 273)) + Convert.ToDouble(fbien.b)))).ToString();
                double P0 = 0.98067 * Math.Pow(10, ((Convert.ToDouble(fbien.a) / (Convert.ToDouble(fbien.t0) + 273)) + Convert.ToDouble(fbien.b)));
                fbien.P0 = PreciseDecimalValue(P0, 3).ToString();
            }
            catch (Exception)
            {

                ToolTip toolTipt2 = new ToolTip();
                toolTipt2.ShowAlways = true;
                toolTipt2.IsBalloon = true;
                toolTipt2.SetToolTip(textBox4, "Xin hãy nhập đúng dữ liệu");
            }
        }

        // Function tinh toán điểm nút
        /// <summary>
        ///ĐIỂM 10
        /// </summary>
        public void diem10()
        {
            fbien.P10 = fbien.P0;
            fbien.t10 = fbien.tK;
            //D16 =t10; Convert.ToDouble(fbien.t10)
            //D15 = P10; Convert.ToDouble(fbien.P10)
            //D17 = Z10
            double Z6, Z5, Z4, Z3, Z2, Z1, cont, in10, Ina10, qhh10;
            Z6 = -14361.4 / (Convert.ToDouble(fbien.t10) + 273) + 18.41133;
            Z5 = 55286.5 / (Convert.ToDouble(fbien.t10) + 273) - 82.71063;
            Z4 = -80989.9 / (Convert.ToDouble(fbien.t10) + 273) + 135.7893;
            Z3 = 56507 / (Convert.ToDouble(fbien.t10) + 273) + -102.9123;
            Z2 = -20228.3 / (Convert.ToDouble(fbien.t10) + 273) + 37.90183;
            Z1 = 4669.96 / (Convert.ToDouble(fbien.t10) + 273) - 7.0317;
            cont = -2103.5 / (Convert.ToDouble(fbien.t10) + 273) + 5.65208 - Math.Log10(Convert.ToDouble(fbien.P10) / 0.98067);
            fbien.Z10 = classHam.hammusau(Z6, Z5, Z4, Z3, Z2, Z1, cont, 0.1, 0.8, 0.0001);
            //=1039,5+3,56393*(D16+273)+0,919594*10^(-3)*(D16+273)^2+0,124376*D15- 0,222979*10^(-5)*D15^2- 0,35696*D15*(D16+273)^2
            in10 = 1039.5 + 3.56393 * (Convert.ToDouble(fbien.t10) + 273) + 0.919594 * Math.Pow(10, -3) * Math.Pow((Convert.ToDouble(fbien.t10) + 273), 2) + 0.124376 * Convert.ToDouble(fbien.P10) - 0.222979 * Math.Pow(10, -5) * Math.Pow(Convert.ToDouble(fbien.P10), 2) - 0.35696 * Convert.ToDouble(fbien.P10) * Math.Pow((Convert.ToDouble(fbien.t10) + 273), 2);
            //=10580,43+3,02148*(273+D16)+0,312072*10^(-2)*(273+D16)^2+0,15536*D15-0,123079*10^(-4)*D15^2-0,12946*10^(-5)*D15*(273+D16)^2
            Ina10 = 10580.43 + 3.02148 * (273 + Convert.ToDouble(fbien.t10)) + 0.312072 * Math.Pow(10, -2) * Math.Pow((273 + Convert.ToDouble(fbien.t10)), 2) + 0.15536 * Convert.ToDouble(fbien.P10) - 0.123079 * Math.Pow(10, -4) * Math.Pow(Convert.ToDouble(fbien.P10), 2) - 0.12946 * Math.Pow(10, -5) * Convert.ToDouble(fbien.P10) * Math.Pow((273 + Convert.ToDouble(fbien.t10)), 2);
            //=-839,87*(1-0,071*D17)*((1-D17)*D17+((1-D17)*D17)^2)
            qhh10 = -839.87 * (1 - 0.071 * Convert.ToDouble(fbien.Z10)) * ((1 - Convert.ToDouble(fbien.Z10)) * Convert.ToDouble(fbien.Z10) + Math.Pow(((1 - Convert.ToDouble(fbien.Z10)) * Convert.ToDouble(fbien.Z10)), 2));
            //=-((1-D17)*I21+D17*K21-M21)/1000
            fbien.i10 = (-((1 - Convert.ToDouble(fbien.Z10)) * in10 + Convert.ToDouble(fbien.Z10) * Ina10 - qhh10) / 1000).ToString();

        }
        // Function tinh toán điểm nút
        /// <summary>
        ///ĐIỂM 7
        /// </summary>
        public void diem7()
        {
            fbien.P7 = fbien.Pk;
            fbien.t7 = fbien.tH;
            double Z6, Z5, Z4, Z3, Z2, Z1, cont, in7, Ina7, qhh7;
            //=-14361,4/($D$22+273)+18,41133
            Z6 = -14361.4 / (Convert.ToDouble(fbien.t7) + 273) + 18.41133;
            //=55286,5/($D$22+273)-82,71063
            Z5 = 55286.5 / (Convert.ToDouble(fbien.t7) + 273) - 82.71063;
            //=-80989,9/($D$22+273)+135,7893
            Z4 = -80989.9 / (Convert.ToDouble(fbien.t7) + 273) + 135.7893;
            //=56507/($D$22+273)+-102,9123
            Z3 = 56507 / (Convert.ToDouble(fbien.t7) + 273) + -102.9123;
            //=-20228,3/($D$22+273)+37,90183
            Z2 = -20228.3 / (Convert.ToDouble(fbien.t7) + 273) + 37.90183;
            //=4669,96/($D$22+273)-7,0317
            Z1 = 4669.96 / (Convert.ToDouble(fbien.t7) + 273) - 7.0317;
            //=-2103,5/($D$22+273)+5,65208-LOG($D$21/0,98067)
            cont = -2103.5 / (Convert.ToDouble(fbien.t7) + 273) + 5.65208 - Math.Log10(Convert.ToDouble(fbien.P7) / 0.98067);
            fbien.Z7 = classHam.hammusau(Z6, Z5, Z4, Z3, Z2, Z1, cont, 0.1, 1, 0.0001);

            //=1039,5+3,56393*(D22+273)+0,919594*10^(-3)*(D22+273)^2+0,124376*D21- 0,222979*10^(-5)*D21^2- 0,35696*D21*(D22+273)^2
            in7 = 1039.5 + 3.56393 * (Convert.ToDouble(fbien.t7) + 273) + 0.919594 * Math.Pow(10, -3) * Math.Pow((Convert.ToDouble(fbien.t7) + 273), 2) + 0.124376 * Convert.ToDouble(fbien.P7) - 0.222979 * Math.Pow(10, -5) * Math.Pow(Convert.ToDouble(fbien.P7), 2) - 0.35696 * Convert.ToDouble(fbien.P7) * Math.Pow((Convert.ToDouble(fbien.t7) + 273), 2);
            //=10580,43+3,02148*(273+D22)+0,312072*10^(-2)*(273+D22)^2+0,15536*D21-0,123079*10^(-4)*D21^2-0,12946*10^(-5)*D21*(273+D22)^2
            Ina7 = 10580.43 + 3.02148 * (273 + Convert.ToDouble(fbien.t7)) + 0.312072 * Math.Pow(10, -2) * Math.Pow((273 + Convert.ToDouble(fbien.t7)), 2) + 0.15536 * Convert.ToDouble(fbien.P7) - 0.123079 * Math.Pow(10, -4) * Math.Pow(Convert.ToDouble(fbien.P7), 2) - 0.12946 * Math.Pow(10, -5) * Convert.ToDouble(fbien.P7) * Math.Pow((273 + Convert.ToDouble(fbien.t7)), 2);
            //=-839,87*(1-0,071*D23)*((1-D23)*D23+((1-D23)*D23)^2)
            qhh7 = -839.87 * (1 - 0.071 * Convert.ToDouble(fbien.Z7)) * ((1 - Convert.ToDouble(fbien.Z7)) * Convert.ToDouble(fbien.Z7) + Math.Pow(((1 - Convert.ToDouble(fbien.Z7)) * Convert.ToDouble(fbien.Z7)), 2));
            //=-((1-D23)*I25+D23*K25-M25)/1000
            fbien.i7 = (-((1 - Convert.ToDouble(fbien.Z7)) * in7 + Convert.ToDouble(fbien.Z7) * Ina7 - qhh7) / 1000).ToString();
            fbien.DZ = (Convert.ToDouble(fbien.Z10) - Convert.ToDouble(fbien.Z7)).ToString();
        }
        // Function tinh toán điểm nút
        /// <summary>
        ///ĐIỂM 2
        /// </summary>
        public void diem2()
        {
            fbien.P2 = fbien.Pk;
            fbien.t2 = fbien.tK;
            fbien.Z2 = fbien.Zd;
            // Nội suy từ bảng trong DB
            DataTable dt_d2 = new DataTable();
            dt_d2 = classHam.checkBanghoinh3(Convert.ToDouble(fbien.t2)) ? classHam.getBanghoinh3_noisuy_xd(Convert.ToDouble(fbien.t2)) : classHam.getBanghoinh3_noisuy_kxd(Convert.ToDouble(fbien.t2));
            if (dt_d2.Rows.Count > 0)
            {
                DataRow row = dt_d2.Rows[0];
                DataRow row1 = dt_d2.Rows[1];
                double pa_d2, pb_d2, ha_d2, hb_d2;
                pa_d2 = Convert.ToDouble(row["apsuat"]);
                pb_d2 = Convert.ToDouble(row1["apsuat"]);
                ha_d2 = Convert.ToDouble(row["entanpi_long"]);
                hb_d2 = Convert.ToDouble(row1["entanpi_long"]);
                fbien.i2 = (ha_d2 + (hb_d2 - ha_d2) * (Convert.ToDouble(fbien.P2) - pa_d2) / (hb_d2 - pa_d2)).ToString();
            }
            else
            {
                MessageBox.Show("Có lỗi trong quá trình lấy dữ liệu bảng BanghoiNH3");
            }

        }
        /// <summary>
        ///ĐIỂM 5
        /// </summary>
        public void diem5()
        {
            fbien.P5 = fbien.P0;
            fbien.t5 = fbien.t0;
            fbien.Z5 = fbien.Zd;
            // Nội suy từ bảng trong DB
            DataTable dt_d5 = new DataTable();
            dt_d5 = classHam.checkBanghoinh3(Convert.ToDouble(fbien.t5)) ? classHam.getBanghoinh3_noisuy_xd(Convert.ToDouble(fbien.t5)) : classHam.getBanghoinh3_noisuy_kxd(Convert.ToDouble(fbien.t5));
            if (dt_d5.Rows.Count > 0)
            {
                DataRow row = dt_d5.Rows[0];
                DataRow row1 = dt_d5.Rows[1];
                double pa_d5, pb_d5, ha_d5, hb_d5;
                pa_d5 = Convert.ToDouble(row["apsuat"]);
                pb_d5 = Convert.ToDouble(row1["apsuat"]);
                ha_d5 = Convert.ToDouble(row["entanpi_hoi"]);
                hb_d5 = Convert.ToDouble(row1["entanpi_hoi"]);
                fbien.i5 = (ha_d5 + (hb_d5 - ha_d5) * (Convert.ToDouble(fbien.P5) - pa_d5) / (hb_d5 - pa_d5)).ToString();
            }
            else
            {
                MessageBox.Show("Có lỗi trong quá trình lấy dữ liệu bảng BanghoiNH3");
            }

        }
        /// <summary>
        ///ĐIỂM 6
        /// </summary>
        public void diem6()
        {
            fbien.P6 = fbien.P0;
            fbien.t6 = (Convert.ToDouble(fbien.t2) - 5).ToString();
            fbien.Z6 = fbien.Z5;
            //=770,761+1,86947*(D43+273)+0,587293*10^(-4)*(D43+273)^2+0,731509*10^(-6)*(D43+273)^3+8,98074*D42-4580,15*(D42/(D43+273))
            fbien.i6 = (770.761 + 1.86947 * (Convert.ToDouble(fbien.t6) + 273) + 0.587293 * Math.Pow(10, -4) * Math.Pow((Convert.ToDouble(fbien.t6) + 273), 2) + 0.731509 * Math.Pow(10, -6) * Math.Pow((Convert.ToDouble(fbien.t6) + 273), 3) + 8.98074 * Convert.ToDouble(fbien.P6) - 4580.15 * (Convert.ToDouble(fbien.P6) / (Convert.ToDouble(fbien.t6) + 273))).ToString();
        }
        /// <summary>
        ///ĐIỂM 3
        /// </summary>
        public void diem3()
        {
            fbien.P3 = fbien.Pk;
            fbien.Z3 = fbien.Zd;
            DataTable dt_d3 = new DataTable();
            dt_d3 = classHam.checkBangnh3(Convert.ToDouble(fbien.t2)) ? classHam.getBangnh3_noisuy_xd(Convert.ToDouble(fbien.t2)) : classHam.getBangnh3_noisuy_kxd(Convert.ToDouble(fbien.t2));
            if (dt_d3.Rows.Count > 0)
            {
                DataRow row = dt_d3.Rows[0];
                DataRow row1 = dt_d3.Rows[1];
                double pa_d3, pb_d3, Cph1_d2, Cph2_d2, Cpl1_d2, Cpl2_d2, Cph_ns1, Cph_ns2;
                pa_d3 = Convert.ToDouble(row["p"]);
                pb_d3 = Convert.ToDouble(row1["p"]);
                Cph1_d2 = Convert.ToDouble(row["c2"]);
                Cph2_d2 = Convert.ToDouble(row1["c2"]);
                Cpl1_d2 = Convert.ToDouble(row["c1"]);
                Cpl2_d2 = Convert.ToDouble(row1["c1"]);
                //=L47+(L48-L47)*($D$48-I47)/(I48-I47)
                Cph_ns1 = Cph1_d2 + (Cph2_d2 - Cph1_d2) * (Convert.ToDouble(fbien.P3) - pa_d3) / (pb_d3 - pa_d3);
                //=L50+(L51-L50)*($D$48-I50)/(I51-I50)
                Cph_ns2 = Cpl1_d2 + (Cpl2_d2 - Cpl1_d2) * (Convert.ToDouble(fbien.P3) - pa_d3) / (pb_d3 - pa_d3);
                //=D30-(L49*(D43-D36)/L52)
                fbien.t3 = (Convert.ToDouble(fbien.t2) - (Cph_ns1 * (Convert.ToDouble(fbien.t6) - Convert.ToDouble(fbien.t5)) / Cph_ns2)).ToString();
                //=D32+D38-D45
                fbien.i3 = (Convert.ToDouble(fbien.i2) + Convert.ToDouble(fbien.i5) - Convert.ToDouble(fbien.i6)).ToString();
            }
            else
            {
                MessageBox.Show("Có lỗi trong quá trình lấy dữ liệu bảng BangNH3");
            }
        }
        /// <summary>
        ///ĐIỂM 4
        /// </summary>
        public void diem4()
        {
            fbien.P4 = fbien.Pk;
            fbien.t4 = fbien.t0;
            fbien.Z4 = fbien.Zd;
            fbien.i4 = fbien.i3;
        }
        /// <summary>
        ///ĐIỂM 11
        /// </summary>
        public void diem11()
        {
            fbien.P11 = fbien.Pk;
            fbien.t11 = fbien.t10;
            fbien.Z11 = fbien.Z10;
            fbien.i11 = fbien.i10;
            double i67, j67, j66;
            //=-6571,06+39,9544*(D7+273)-0,243781*10^-2*(D7+273)^2+0,39792*10^-6*(D7+273)^3-22,7722*D11-0,4979*10^-7*D11^2+8286,02*(D11/(D7+273))+0,172363*0,1*D11*(D7+273)+0,77344*10^-6*D11*(D7+273)^2-3,62962*(D7+273)*LN(D7+273)-(D7+273)*LN(D11)
            i67 = -6571.06 + 39.9544 * (Convert.ToDouble(fbien.tH) + 273) - 0.243781 * Math.Pow(10, -2) * Math.Pow((Convert.ToDouble(fbien.tH) + 273), 2) + 0.39792 * Math.Pow(10, -6) * Math.Pow((Convert.ToDouble(fbien.tH) + 273), 3) - 22.7722 * Convert.ToDouble(fbien.Pk) - 0.4979 * Math.Pow(10, -7) * Math.Pow(Convert.ToDouble(fbien.Pk), 2) + 8286.02 * (Convert.ToDouble(fbien.Pk) / (Convert.ToDouble(fbien.tH) + 273)) + 0.172363 * 0.1 * Convert.ToDouble(fbien.Pk) * (Convert.ToDouble(fbien.tH) + 273) + 0.77344 * Math.Pow(10, -6) * Convert.ToDouble(fbien.Pk) * Math.Pow((Convert.ToDouble(fbien.tH) + 273), 2) - 3.62962 * (Convert.ToDouble(fbien.tH) + 273) * Math.Log(Convert.ToDouble(fbien.tH) + 273) - (Convert.ToDouble(fbien.tH) + 273) * Math.Log(Convert.ToDouble(fbien.Pk));
            //=-1467*D62^2*(1,25*D62^3-5,58*D62^2+5,96*D62-0,42)+(17,3*D62^3-13,8*D62^4)*(D7+273)
            j67 = -1467 * Math.Pow(Convert.ToDouble(fbien.Z11), 2) * (1.25 * Math.Pow(Convert.ToDouble(fbien.Z11), 3) - 5.58 * Math.Pow(Convert.ToDouble(fbien.Z11), 2) + 5.96 * Convert.ToDouble(fbien.Z11) - 0.42) + (17.3 * Math.Pow(Convert.ToDouble(fbien.Z11), 3) - 13.8 * Math.Pow(Convert.ToDouble(fbien.Z11), 4)) * (Convert.ToDouble(fbien.tH) + 273);
            //= (I67 + J67) / (D7 + 273)
            j66 = (i67 + j67) / (Convert.ToDouble(fbien.tH) + 273);
            //=1-(1-D62)*EXP(1)^J66
            fbien.Zh = (1 - (1 - Convert.ToDouble(fbien.Z11)) * Math.Pow(Math.Exp(1), j66)).ToString();
        }
        /// <summary>
        ///ĐIỂM 14
        /// </summary>
        public void diem14()
        {
            fbien.P14 = fbien.P11;
            fbien.t14 = fbien.tH;
            fbien.Z14 = fbien.Zh;
            //=(1-D71)*(1993,19+1,88878*(D70+273)-0,205512*10^-3*(D70+273)^2+0,367295*10^-6*(D70+273)^3+10,6342*D69-7648,34*D69/(D70+273))+D71*(770,761+1,86947*(D70+273)+0,578293*10^-4*(D70+273)^2+0,731509*10^-6*(D70+273)^3+8,98074*D69-4580,15*D69/(D70+273))
            fbien.i14 = ((1 - Convert.ToDouble(fbien.Z14)) * (1993.19 + 1.88878 * (Convert.ToDouble(fbien.t14) + 273) - 0.205512 * Math.Pow(10, -3) * Math.Pow((Convert.ToDouble(fbien.t14) + 273), 2) + 0.367295 * Math.Pow(10, -6) * Math.Pow((Convert.ToDouble(fbien.t14) + 273), 3) + 10.6342 * Convert.ToDouble(fbien.P14) - 7648.34 * Convert.ToDouble(fbien.P14) / (Convert.ToDouble(fbien.t14) + 273)) + Convert.ToDouble(fbien.Z14) * (770.761 + 1.86947 * (Convert.ToDouble(fbien.t14) + 273) + 0.578293 * Math.Pow(10, -4) * Math.Pow((Convert.ToDouble(fbien.t14) + 273), 2) + 0.731509 * Math.Pow(10, -6) * Math.Pow((Convert.ToDouble(fbien.t14) + 273), 3) + 8.98074 * Convert.ToDouble(fbien.P14) - 4580.15 * Convert.ToDouble(fbien.P14) / (Convert.ToDouble(fbien.t14) + 273))).ToString();
        }
        /// <summary>
        ///ĐIỂM 15
        /// </summary>
        public void diem15()
        {
            fbien.P15 = fbien.Pk;
            fbien.Z15 = "0";
            fbien.rH2O = (((1 - Convert.ToDouble(fbien.Zh)) / 18) / (((1 - Convert.ToDouble(fbien.Zh)) / 18) + (Convert.ToDouble(fbien.Zh) / 17))).ToString();
            fbien.PH2O = (Convert.ToDouble(fbien.rH2O) * Convert.ToDouble(fbien.P15)).ToString();

            DataTable dt_d15 = new DataTable();
            double bien_ph20;

            if (Convert.ToDouble(fbien.PH2O) >= 0.3)
            {
                bien_ph20 = Convert.ToDouble(PreciseDecimalValue(Convert.ToDouble(fbien.PH2O), 1));
            }
            else
            {
                bien_ph20 = Convert.ToDouble(PreciseDecimalValue(Convert.ToDouble(fbien.PH2O), 2));
            }
            dt_d15 = classHam.getBangh2o_noisuy_kxd(bien_ph20);
            if (dt_d15.Rows.Count > 0)
            {
                DataRow row = dt_d15.Rows[0];
                DataRow row1 = dt_d15.Rows[1];
                double pa_d15, pb_d15, t1_d15, t2_d15, i1_d15, i2_d15;
                pa_d15 = Convert.ToDouble(row["p"]);
                pb_d15 = Convert.ToDouble(row1["p"]);
                t1_d15 = Convert.ToDouble(row["t"]);
                t2_d15 = Convert.ToDouble(row1["t"]);
                i1_d15 = Convert.ToDouble(row["h1"]);
                i2_d15 = Convert.ToDouble(row1["h1"]);

                //=L74+(L75-L74)*($D$79-I74)/(I75-I74)
                fbien.t15 = (t1_d15 + (t2_d15 - t1_d15) * (Convert.ToDouble(fbien.PH2O) - pa_d15) / (pb_d15 - pa_d15)).ToString();
                //=L77+(L78-L77)*($D$79-I77)/(I78-I77)
                fbien.i15 = (i1_d15 + (i2_d15 - i1_d15) * (Convert.ToDouble(fbien.PH2O) - pa_d15) / (pb_d15 - pa_d15)).ToString();

            }
            else
            {
                MessageBox.Show("Có lỗi trong quá trình lấy dữ liệu bảng Bang H2O");
            }
        }
        /// <summary>
        ///ĐIỂM 1
        /// </summary>
        public void diem1()
        {
            fbien.P1 = fbien.Pk;
            fbien.t1 = fbien.t15;
            fbien.Z1 = "1";
            //=770,761+1,86947*(D84+273)+0,587293*10^-4*(D84+273)^2+0,731509*10^-6*(D84+273)^3+8,98074*D83-4580,15*D83/(D84+273)
            fbien.i1 = (770.761 + 1.86947 * (Convert.ToDouble(fbien.t1) + 273) + 0.587293 * Math.Pow(10, -4) * Math.Pow((Convert.ToDouble(fbien.t1) + 273), 2) + 0.731509 * Math.Pow(10, -6) * Math.Pow((Convert.ToDouble(fbien.t1) + 273), 3) + 8.98074 * Convert.ToDouble(fbien.P1) - 4580.15 * Convert.ToDouble(fbien.P1) / (Convert.ToDouble(fbien.t1) + 273)).ToString();
        }
        /// <summary>
        ///ĐIỂM 12
        /// </summary>
        public void diem12()
        {
            fbien.P12 = fbien.Pk;
            fbien.Z12 = fbien.Z11;
            double a, b, c, r, mr, delta;
            mr = (1 - Convert.ToDouble(fbien.Z7)) / (Convert.ToDouble(fbien.Z11) - Convert.ToDouble(fbien.Z7));
            r = (1 - Convert.ToDouble(fbien.Z14)) / (Convert.ToDouble(fbien.Z14) - Convert.ToDouble(fbien.Z11));
            fbien.i12 = (((1 + r) * Convert.ToDouble(fbien.i14) - Convert.ToDouble(fbien.i1) - r * Convert.ToDouble(fbien.i15)) / mr + Convert.ToDouble(fbien.i11)).ToString();
            //D62 Convert.ToDouble(fbien.Z11)
            //D61 Convert.ToDouble(fbien.t11)
            //D88 Convert.ToDouble(fbien.P12)
            //=1,83918*10^-3 + 7,13992*10^-7*D88 + 4,40232*10^-3*D62 - 1,87528*10^-6*D88*D62
            a = 1.83918 * Math.Pow(10, -3) + 7.13992 * Math.Pow(10, -7) * Convert.ToDouble(fbien.P12) + 4.40232 * Math.Pow(10, -3) * Convert.ToDouble(fbien.Z11) - 1.87528 * Math.Pow(10, -6) * Convert.ToDouble(fbien.P12) * Convert.ToDouble(fbien.Z11);
            //=3,56393-0,54245*D62-(D61+273)*(1,83918*10^-3+7,13992*10^-7*D88+4,40232*10^-3*D62-1,87528*10^-6*D88*D62)
            b = 3.56393 - 0.54245 * Convert.ToDouble(fbien.Z11) - (Convert.ToDouble(fbien.t11) + 273) * (1.83918 * Math.Pow(10, -3) + 7.13992 * Math.Pow(10, -7) * Convert.ToDouble(fbien.P12) + 4.40232 * Math.Pow(10, -3) * Convert.ToDouble(fbien.Z11) - 1.87528 * Math.Pow(10, -6) * Convert.ToDouble(fbien.P12) * Convert.ToDouble(fbien.Z11));
            //=D63-D91-(273+D61)*(3,56393-0,54245*D62)
            c = Convert.ToDouble(fbien.i11) - Convert.ToDouble(fbien.i12) - (273 + Convert.ToDouble(fbien.t11)) * (3.56393 - 0.54245 * Convert.ToDouble(fbien.Z11));
            delta = Math.Pow(b, 2) - 4 * a * c;
            fbien.t12 = ((-b + Math.Sqrt(delta)) / (2 * a) - 273).ToString();
        }
        /// <summary>
        ///ĐIỂM 8
        /// </summary>
        public void diem8()
        {
            fbien.P8 = fbien.P12;
            fbien.t8 = (Convert.ToDouble(fbien.t12) + 5).ToString();
            fbien.Z8 = fbien.Z7;
            //=D24-(3.56393+1.83918*10^-3*(273+D94)+7.13992*10^-7*D93*(273+D94)-(0.54245-4.40232*10^-3*(273+D94)+1.87528*10^-6*D93*(273+D94))*D95)*(D22-D94)
            fbien.i8 = (Convert.ToDouble(fbien.i7) - (3.56393 + 1.83918 * Math.Pow(10, -3) * (273 + Convert.ToDouble(fbien.t8)) + 7.13992 * Math.Pow(10, -7) * Convert.ToDouble(fbien.P8) * (273 + Convert.ToDouble(fbien.t8)) - (0.54245 - 4.40232 * Math.Pow(10, -3) * (273 + Convert.ToDouble(fbien.t8)) + 1.87528 * Math.Pow(10, -6) * Convert.ToDouble(fbien.P8) * (273 + Convert.ToDouble(fbien.t8))) * Convert.ToDouble(fbien.Z8)) * (Convert.ToDouble(fbien.t7) - Convert.ToDouble(fbien.t8))).ToString();
        }
        /// <summary>
        ///ĐIỂM 13
        /// </summary>
        public void diem13()
        {
            fbien.P13 = fbien.P8;
            fbien.Z13 = fbien.Z11;


            double a, b, c, r, mr, delta;
            mr = (1 - Convert.ToDouble(fbien.Z7)) / (Convert.ToDouble(fbien.Z11) - Convert.ToDouble(fbien.Z7));
            r = (1 - Convert.ToDouble(fbien.Z14)) / (Convert.ToDouble(fbien.Z14) - Convert.ToDouble(fbien.Z11));

            fbien.i13 = (((mr - 1) / mr) * (Convert.ToDouble(fbien.i7) - Convert.ToDouble(fbien.i8)) + Convert.ToDouble(fbien.i12)).ToString();

            //D62 Convert.ToDouble(fbien.Z11)
            //D61 Convert.ToDouble(fbien.t11)
            //D88 Convert.ToDouble(fbien.P12)
            //=I90*(1,83918*10^-3+7,13992*10^-7*D98+(4,40232*10^-3-1,87528*10^-6*D98)*D100)
            a = mr * (1.83918 * Math.Pow(10, -3) + 7.13992 * Math.Pow(10, -7) * Convert.ToDouble(fbien.P13) + (4.40232 * Math.Pow(10, -3) - 1.87528 * Math.Pow(10, -6) * Convert.ToDouble(fbien.P13)) * Convert.ToDouble(fbien.Z13));
            //=I90*(3,56393-0,54245*D100)-((1,83918*10^-3+7,13992*10^-7*D98)*((I90-1)*(D22-D94)+(D89+273)*I90)+(4,40232*10^-3-1,87528*10^-6*D98)*((I90-1)*(D22-D94)*D95+(D89+273)*I90*D100))
            b = mr * (3.56393 - 0.54245 * Convert.ToDouble(fbien.Z13)) - ((1.83918 * Math.Pow(10, -3) + 7.13992 * Math.Pow(10, -7) * Convert.ToDouble(fbien.P13)) * ((mr - 1) * (Convert.ToDouble(fbien.t7) - Convert.ToDouble(fbien.t8)) + (Convert.ToDouble(fbien.t12) + 273) * mr) + (4.40232 * Math.Pow(10, -3) - 1.87528 * Math.Pow(10, -6) * Convert.ToDouble(fbien.P13)) * ((mr - 1) * (Convert.ToDouble(fbien.t7) - Convert.ToDouble(fbien.t8)) * Convert.ToDouble(fbien.Z8) + (Convert.ToDouble(fbien.t12) + 273) * mr * Convert.ToDouble(fbien.Z13)));
            //=-(I90-1)*(D22-D94)*(3,56393-0,54245*D95)-(273+D89)*I90*(3,56393-0,54245*D100)
            c = -(mr - 1) * (Convert.ToDouble(fbien.t7) - Convert.ToDouble(fbien.t8)) * (3.56393 - 0.54245 * Convert.ToDouble(fbien.Z8)) - (273 + Convert.ToDouble(fbien.t12)) * mr * (3.56393 - 0.54245 * Convert.ToDouble(fbien.Z13));
            //=K99^2-4*I99*M99
            delta = Math.Pow(b, 2) - 4 * a * c;
            //=(-K99+SQRT(O99))/(2*I99)-273
            fbien.t13 = ((-b + Math.Sqrt(delta)) / (2 * a) - 273).ToString();
        }
        /// <summary>
        ///ĐIỂM 9
        /// </summary>
        public void diem9()
        {
            fbien.P9 = fbien.P0;
            fbien.t9 = fbien.t8;
            fbien.Z9 = fbien.Z8;
            fbien.i9 = fbien.i8;
        }
        void TieptheoClick(object sender, EventArgs e)
        {

        }
        // CLick tab mô phỏng điểm nút
        private void t1_Click(object sender, EventArgs e)
        {
            try
            {
                //t1.SelectedTab = tabPage2;
                khoitao();
            }
            catch (Exception)
            {
            }
        }
        void Btn_tieptheoClick(object sender, EventArgs e)
        {
            t1.SelectedTab = tabPage3;

        }
        void Btn_quaylaiClick(object sender, EventArgs e)
        {
            t1.SelectedTab = tabPage1;
        }
        private decimal RoundDownTo2DecimalPlaces(decimal input)
        {
            if (input < 0)
            {
                throw new Exception("not tested with negitive numbers");
            }

            // There must be a better way!
            return Math.Truncate(input * 100) / 100;
        }
        public decimal RoundDownTo3DecimalPlaces(decimal input)
        {
            if (input < 0)
            {
                throw new Exception("not tested with negitive numbers");
            }

            // There must be a better way!
            return Math.Truncate(input * 1000) / 1000;
        }
        public string PreciseDecimalValue(double Value, int DigitsAfterDecimal)
        {

            string PreciseDecimalFormat = "{0:0.0}";

            for (int count = 2; count <= DigitsAfterDecimal; count++)
            {
                PreciseDecimalFormat = PreciseDecimalFormat.Insert(PreciseDecimalFormat.LastIndexOf('}'), "0");
            }
            return String.Format(PreciseDecimalFormat, Value);

        }

        public string lay3so(string value, int digit)
        {
            if (IsNumber(value.ToString()))
            {
                return value.ToString();
            }
            else
            {
                string PreciseDecimalFormat = "{0:0.0}";

                for (int count = 2; count <= digit; count++)
                {
                    PreciseDecimalFormat = PreciseDecimalFormat.Insert(PreciseDecimalFormat.LastIndexOf('}'), "0");
                }
                return String.Format(PreciseDecimalFormat, Convert.ToDouble(value));
            }
        }
        public bool IsNumber(string pValue)
        {
            foreach (Char c in pValue)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }


        private void dn10_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip10 = new ToolTip();
            toolTip10.ShowAlways = true;
            toolTip10.IsBalloon = true;
            toolTip10.AutoPopDelay = 1000000;
            toolTip10.ToolTipTitle = "Điểm nút 10";

            toolTip10.SetToolTip(dn10, "Nhiệt độ: " + lay3so(fbien.t10, 3) + " \nÁp suất: " + lay3so(fbien.P10, 3) + " \nEnthanlpy: " + lay3so(fbien.i10, 3) + " \nNồng độ dung dịch: " + lay3so(fbien.Z10, 3) + "");

        }

        private void dn7_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip7 = new ToolTip();
            toolTip7.ShowAlways = true;
            toolTip7.IsBalloon = true;
            toolTip7.AutoPopDelay = 1000000;
            toolTip7.ToolTipTitle = "Điểm nút 7";

            toolTip7.SetToolTip(dn7, "Nhiệt độ: " + lay3so(fbien.t7,3) + " \nÁp suất: " + lay3so(fbien.P7,3) + " \nEnthanlpy: " + lay3so(fbien.i7,3) + " \nNồng độ dung dịch: " + lay3so(fbien.Z7,3) + "");
        }
        void Dn2MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip2 = new ToolTip();
            toolTip2.ShowAlways = true;
            toolTip2.IsBalloon = true;
            toolTip2.AutoPopDelay = 1000000;
            toolTip2.ToolTipTitle = "Điểm nút 2";

            toolTip2.SetToolTip(dn2, "Nhiệt độ: " + lay3so(fbien.t2,3) + " \nÁp suất: " + lay3so(fbien.P2,3) + " \nEnthanlpy: " + lay3so(fbien.i2,3) + " \nNồng độ dung dịch: " + lay3so(fbien.Z2,3) + "");
        }
        private void btn_danhgiaht_Click(object sender, EventArgs e)
        {
            if (Convert.ToDouble(fbien.DZ) > 0)
            {
                MessageBox.Show("Chu trình máy lạnh hấp thụ hoạt động được");
            }
            else
            {
                MessageBox.Show("Chu trình máy lạnh hấp thụ không hoạt động được");
            }
        }
        //connectdata(@"Data Source = " + sqConnectionString + ";Version=3;");
        private void connectdata()
        {
            conn = new SQLiteConnection(@"Data Source = DB_DuLieu.sqlite;Version=3;");
        }

        private void dn5_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip5 = new ToolTip();
            toolTip5.ShowAlways = true;
            toolTip5.IsBalloon = true;
            toolTip5.AutoPopDelay = 1000000;
            toolTip5.ToolTipTitle = "Điểm nút 5";

            toolTip5.SetToolTip(dn5, "Nhiệt độ: " + lay3so(fbien.t5,3) + " \nÁp suất: " + lay3so(fbien.P5,3) + " \nEnthanlpy: " + lay3so(fbien.i5,3) + " \nNồng độ dung dịch: " + lay3so(fbien.Z5,3) + "");

        }

        private void dn6_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip5 = new ToolTip();
            toolTip5.ShowAlways = true;
            toolTip5.IsBalloon = true;
            toolTip5.AutoPopDelay = 1000000;
            toolTip5.ToolTipTitle = "Điểm nút 6";

            toolTip5.SetToolTip(dn6, "Nhiệt độ: " + lay3so(fbien.t6,3) + " \nÁp suất: " + lay3so(fbien.P6,3) + " \nEnthanlpy: " + lay3so(fbien.i6,3) + " \nNồng độ dung dịch: " + lay3so(fbien.Z6,3) + "");
        }

        private void dn3_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip3 = new ToolTip();
            toolTip3.ShowAlways = true;
            toolTip3.IsBalloon = true;
            toolTip3.AutoPopDelay = 1000000;
            toolTip3.ToolTipTitle = "Điểm nút 3";

            toolTip3.SetToolTip(dn3, "Nhiệt độ: " + lay3so(fbien.t3,3) + " \nÁp suất: " + lay3so(fbien.P3,3) + " \nEnthanlpy: " + lay3so(fbien.i3,3) + " \nNồng độ dung dịch: " + lay3so(fbien.Z3,3) + "");

        }

        private void dn4_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip4 = new ToolTip();
            toolTip4.ShowAlways = true;
            toolTip4.IsBalloon = true;
            toolTip4.AutoPopDelay = 1000000;
            toolTip4.ToolTipTitle = "Điểm nút 4";

            toolTip4.SetToolTip(dn4, "Nhiệt độ: " + lay3so(fbien.t4,3) + " \nÁp suất: " + lay3so(fbien.P4,3) + " \nEnthanlpy: " + lay3so(fbien.i4,3) + " \nNồng độ dung dịch: " + lay3so(fbien.Z4,3) + "");

        }

        private void dn11_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip11 = new ToolTip();
            toolTip11.ShowAlways = true;
            toolTip11.IsBalloon = true;
            toolTip11.AutoPopDelay = 1000000;
            toolTip11.ToolTipTitle = "Điểm nút 4";

            toolTip11.SetToolTip(dn11, "Nhiệt độ: " + lay3so(fbien.t11,3) + " \nÁp suất: " + lay3so(fbien.P11,3) + " \nEnthanlpy: " + lay3so(fbien.i11,3) + " \nNồng độ dung dịch: " + lay3so(fbien.Z11,3) + "");
        }

        private void dn14_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip14 = new ToolTip();
            toolTip14.ShowAlways = true;
            toolTip14.IsBalloon = true;
            toolTip14.AutoPopDelay = 1000000;
            toolTip14.ToolTipTitle = "Điểm nút 14";

            toolTip14.SetToolTip(dn14, "Nhiệt độ: " + lay3so(fbien.t14,3) + " \nÁp suất: " + lay3so(fbien.P14,3) + " \nEnthanlpy: " + lay3so(fbien.i14,3) + " \nNồng độ dung dịch: " + lay3so(fbien.Z14,3) + "");
        }

        private void dn15_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip15 = new ToolTip();
            toolTip15.ShowAlways = true;
            toolTip15.IsBalloon = true;
            toolTip15.AutoPopDelay = 1000000;
            toolTip15.ToolTipTitle = "Điểm nút 15";

            toolTip15.SetToolTip(dn15, "Nhiệt độ: " + lay3so(fbien.t15,3) + " \nÁp suất: " + lay3so(fbien.P15,3) + " \nEnthanlpy: " + lay3so(fbien.i15,3) + " \nNồng độ dung dịch: " + lay3so(fbien.Z15,3) + "");
        }
        void Dn1MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip1 = new ToolTip();
            toolTip1.ShowAlways = true;
            toolTip1.IsBalloon = true;
            toolTip1.AutoPopDelay = 1000000;
            toolTip1.ToolTipTitle = "Điểm nút 1";
            toolTip1.SetToolTip(dn1, "Nhiệt độ: " + lay3so(fbien.t1, 3) + " \nÁp suất: " + lay3so(fbien.P1, 3) + " \nEnthanlpy: " + lay3so(fbien.i1, 3) + " \nNồng độ dung dịch: " + lay3so(fbien.Z1, 3) + "");
        }

        private void dn12_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip12 = new ToolTip();
            toolTip12.ShowAlways = true;
            toolTip12.IsBalloon = true;
            toolTip12.AutoPopDelay = 1000000;
            toolTip12.ToolTipTitle = "Điểm nút 12";
            toolTip12.SetToolTip(dn12, "Nhiệt độ: " + lay3so(fbien.t12, 3) + " \nÁp suất: " + lay3so(fbien.P12, 3) + " \nEnthanlpy: " + lay3so(fbien.i12, 3) + " \nNồng độ dung dịch: " + lay3so(fbien.Z12, 3) + "");
        }

        private void dn8_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip8 = new ToolTip();
            toolTip8.ShowAlways = true;
            toolTip8.IsBalloon = true;
            toolTip8.AutoPopDelay = 1000000;
            toolTip8.ToolTipTitle = "Điểm nút 8";
            toolTip8.SetToolTip(dn8, "Nhiệt độ: " + lay3so(fbien.t8, 3) + " \nÁp suất: " + lay3so(fbien.P8, 3) + " \nEnthanlpy: " + lay3so(fbien.i8, 3) + " \nNồng độ dung dịch: " + lay3so(fbien.Z8,3) + "");
        }

        private void dn13_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip13 = new ToolTip();
            toolTip13.ShowAlways = true;
            toolTip13.IsBalloon = true;
            toolTip13.AutoPopDelay = 1000000;
            toolTip13.ToolTipTitle = "Điểm nút 13";
            toolTip13.SetToolTip(dn13, "Nhiệt độ: " + lay3so(fbien.t13, 3) + " \nÁp suất: " + lay3so(fbien.P13, 3) + " \nEnthanlpy: " + lay3so(fbien.i13, 3) + " \nNồng độ dung dịch: " + lay3so(fbien.Z13, 3) + "");

        }

        private void dn9_MouseHover(object sender, EventArgs e)
        {
            ToolTip toolTip9 = new ToolTip();
            toolTip9.ShowAlways = true;
            toolTip9.IsBalloon = true;
            toolTip9.AutoPopDelay = 1000000;
            toolTip9.ToolTipTitle = "Điểm nút 9";
            toolTip9.SetToolTip(dn9, "Nhiệt độ: " + fbien.t9 + " \nÁp suất: " + fbien.P9 + " \nEnthanlpy: " + fbien.i9 + " \nNồng độ dung dịch: " + fbien.Z9 + "");

        }
        public static int HamPow(int num, int exp)
        {
            int ket_qua = 1;
            int i;
            for (i = 1; i <= exp; i++)
                ket_qua = ket_qua * num;
            return ket_qua;
        }

        private void thoat_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {
            if (tinhchonbom == tabControl1.SelectedTab)
            {
                // MessageBox.Show("Tab 1");
                btn_quaylai1.Visible = true;
                btn_xdll3.Visible = true;
                btn_thoat1.Visible = true;
            }
            else if (thietbingungtu == tabControl1.SelectedTab)
            {
                // MessageBox.Show("Tab 2");
                btn_quaylai1.Visible = false;
                btn_xdll3.Visible = false;
                btn_thoat1.Visible = false;
            }
            else if (thietbibayhoi == tabControl1.SelectedTab)
            {
                btn_quaylai1.Visible = false;
                btn_xdll3.Visible = false;
                btn_thoat1.Visible = false;
                // MessageBox.Show("Tab 3");
            }
            else if (binhhapthu == tabControl1.SelectedTab)
            {
                btn_quaylai1.Visible = false;
                btn_xdll3.Visible = false;
                btn_thoat1.Visible = false;
                // MessageBox.Show("Tab 4");
            }
            else
            {
                btn_quaylai1.Visible = false;
                btn_xdll3.Visible = false;
                btn_thoat1.Visible = false;
                //MessageBox.Show("Tab 5");
            }
        }

        private void groupBox11_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            t1.SelectedTab = tabPage2;
            khoitao();
        }
        public void khoitao()
        {
            try
            {
                fbien.tH1 = textBox1.Text.ToString();
                fbien.tH2 = (Convert.ToDouble(fbien.tH1) - 2).ToString();
                fbien.tH = (Convert.ToDouble(fbien.tH1) - 5).ToString();

                fbien.tK = textBox4.Text.ToString();
                double Pk = 0.98067 * Math.Pow(10, (Convert.ToDouble(fbien.a) / (Convert.ToDouble(fbien.tK) + 273) + Convert.ToDouble(fbien.b)));
                fbien.Pk = PreciseDecimalValue(Pk, 3).ToString();

                fbien.t0 = textBox5.Text.ToString();

                double P0 = 0.98067 * Math.Pow(10, ((Convert.ToDouble(fbien.a) / (Convert.ToDouble(fbien.t0) + 273)) + Convert.ToDouble(fbien.b)));
                fbien.P0 = PreciseDecimalValue(P0, 3).ToString();

                ////
                diem10();
                diem7();
                diem2();
                diem5();
                diem6();
                diem3();
                diem4();
                diem10();
                diem11();
                diem14();
                diem15();
                diem1();
                diem12();
                diem8();
                diem13();
                diem9();
            }
            catch
            {
                MessageBox.Show("Kiểm tra lại dữ liệu đầu vào !");
            }
        }

        private void btn_quaylai1_Click(object sender, EventArgs e)
        {
            t1.SelectedTab = tabPage2;
        }

        private void btn_thoat1_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = thietbingungtu;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = thietbibayhoi;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tinhchonbom;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = thietbingungtu;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = binhhapthu;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = thietbibayhoi;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tohop;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void button19_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = binhhapthu;
        }

        private void button17_Click(object sender, EventArgs e)
        {

        }

        private void button16_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void button18_Click(object sender, EventArgs e)
        {

        }
    }
}
